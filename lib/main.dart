import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:logger/logger.dart';
import 'package:provider/provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:unifood/data/database/dao/restaurant_dao.dart';
import 'package:unifood/data/database/dao/reviews_dao.dart';
import 'package:unifood/data/database/dao/user_dao.dart';
import 'package:unifood/data/database/database_mapper.dart';
import 'package:unifood/data/network/client/api_client.dart';
import 'package:unifood/data/network/network_mapper.dart';
import 'package:unifood/data/respository/restaurant_repository.dart';
import 'package:unifood/data/respository/review_repository.dart';
import 'package:unifood/data/respository/user_repository.dart';
import 'package:unifood/firebase_options.dart';
import 'package:unifood/presentation/providers/auth_provider.dart';
import 'package:unifood/presentation/providers/connectivity_provider.dart';
import 'package:unifood/presentation/providers/restaurant_provider.dart';
import 'package:unifood/presentation/providers/review_provider.dart';
import 'package:unifood/presentation/screens/home.dart';
import 'package:unifood/routes/route_generator.dart';
import 'package:unifood/theme/app_theme.dart';

void main() async {
  final widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  // ignore: deprecated_member_use
  await Sqflite.devSetDebugModeOn();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  if (kIsWeb) {
    await FirebaseFirestore.instance
        .enablePersistence(const PersistenceSettings(synchronizeTabs: true));
  }
  final Logger log = Logger(
    printer: PrettyPrinter(),
    level: Level.trace,
  );

  FirebaseAnalytics.instance.setAnalyticsCollectionEnabled(true);
  FlutterNativeSplash.remove();
  runApp(
    MyApp(
      log: log,
    ),
  );
  FlutterError.onError = (details) {
    FirebaseCrashlytics.instance.recordFlutterFatalError(details);
  };

  PlatformDispatcher.instance.onError = (error, stack) {
    FirebaseCrashlytics.instance.recordError(error, stack, fatal: true);

    return true;
  };
}

class MyApp extends StatelessWidget {
  final Logger log;

  final GlobalKey<ScaffoldMessengerState> _key = GlobalKey();
  MyApp({
    super.key,
    required this.log,
  });

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<ApiClient>(
          create: (context) {
            return ApiClient();
          },
        ),
        Provider<Logger>.value(
          value: log,
        ),
        ProxyProvider<Logger, ConnectivityProvider>(
          update: (context, log, prev) {
            return ConnectivityProvider(log: log, key: _key);
          },
        ),
        ProxyProvider<Logger, NetworkMapper>(
          update: (context, log, previous) {
            return NetworkMapper(log: log);
          },
        ),
        ProxyProvider<Logger, DatabaseMapper>(
          update: (context, log, previous) {
            return DatabaseMapper(log: log);
          },
        ),
        ProxyProvider<Logger, RestaurantsDao>(
          update: (context, log, prev) {
            return RestaurantsDao(log);
          },
        ),
        ProxyProvider6<ApiClient, NetworkMapper, DatabaseMapper, RestaurantsDao,
            ConnectivityProvider, Logger, RestaurantRepository>(
          update: (
            context,
            apiClient,
            networkMapper,
            databaseMapper,
            restaurantsDao,
            connectivityProvider,
            log,
            previous,
          ) {
            return RestaurantRepository(
              apiClient: apiClient,
              networkMapper: networkMapper,
              databaseMapper: databaseMapper,
              restaurantsDao: restaurantsDao,
              connectivityProvider: connectivityProvider,
              log: log,
            );
          },
        ),
        ChangeNotifierProxyProvider2<Logger, RestaurantRepository,
            RestaurantProvider>(
          create: (context) {
            return RestaurantProvider();
          },
          update: (context, log, restaurantRepository, previous) {
            return RestaurantProvider(
              log: log,
              restaurantRepository: restaurantRepository,
            );
          },
        ),
        ProxyProvider<Logger, UsersDao>(
          update: (context, log, prev) {
            return UsersDao(log);
          },
        ),
        ProxyProvider6<ApiClient, NetworkMapper, DatabaseMapper, UsersDao,
            ConnectivityProvider, Logger, UserRepository>(
          update: (
            context,
            apiClient,
            networkMapper,
            databaseMapper,
            userDao,
            connectivityProvider,
            log,
            previous,
          ) {
            return UserRepository(
              apiClient: apiClient,
              networkMapper: networkMapper,
              databaseMapper: databaseMapper,
              usersDao: userDao,
              connectivityProvider: connectivityProvider,
              log: log,
            );
          },
        ),
        ChangeNotifierProxyProvider2<Logger, UserRepository, AuthProvider>(
          create: (context) {
            return AuthProvider();
          },
          update: (context, log, userRepository, previous) {
            return AuthProvider(log: log, userRepository: userRepository);
          },
        ),
        ProxyProvider<Logger, ReviewsDao>(
          update: (context, log, prev) {
            return ReviewsDao(log);
          },
        ),
        ProxyProvider6<ApiClient, NetworkMapper, ReviewsDao, DatabaseMapper,
            ConnectivityProvider, Logger, ReviewRepository>(
          update: (
            context,
            apiClient,
            networkMapper,
            reviewsDao,
            databaseMapper,
            connectivityProvider,
            log,
            previous,
          ) {
            return ReviewRepository(
              apiClient: apiClient,
              networkMapper: networkMapper,
              reviewsDao: reviewsDao,
              databaseMapper: databaseMapper,
              connectivityProvider: connectivityProvider,
              log: log,
            );
          },
        ),
        ChangeNotifierProxyProvider3<Logger, AuthProvider, ReviewRepository,
            ReviewProvider>(
          create: (context) => ReviewProvider(),
          update: (context, log, authProvider, reviewRepository, previous) {
            return ReviewProvider(
              log: log,
              authProvider: authProvider,
              reviewRepository: reviewRepository,
            );
          },
        ),
      ],
      builder: (context, _) {
        return MaterialApp(
          title: 'UniFood',
          scaffoldMessengerKey: _key,
          theme: theme,
          initialRoute: HomePage.path,
          onGenerateRoute: generateRoute,
          debugShowCheckedModeBanner: false,
        );
      },
    );
  }
}
