import 'dart:ui';

import 'package:email_validator/email_validator.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:logger/logger.dart';
import 'package:provider/provider.dart';
import 'package:unifood/domain/exception/network_exception.dart';
import 'package:unifood/presentation/providers/auth_provider.dart';
import 'package:unifood/presentation/screens/forgot_password_screen.dart';
import 'package:unifood/presentation/screens/register_screen.dart';
import 'package:unifood/presentation/widgets/error_snackbar.dart';

class Login extends StatefulWidget {
  static const path = "/login";
  const Login({super.key});

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _key = GlobalKey<FormState>();
  final Map<String, String> _authData = {
    'email': '',
    'password': '',
  };
  bool _isLoading = false;

  Future<void> _login() async {
    final ScaffoldMessengerState scaffoldMessengerState =
        ScaffoldMessenger.of(context);
    final log = context.read<Logger>();
    _key.currentState!.save();

    if (_key.currentState!.validate()) {
      try {
        FocusScope.of(context).unfocus();
        setState(() {
          _isLoading = true;
        });
        await context.read<AuthProvider>().signInWithEmailAndPassword(
              email: _authData['email']!.trim(),
              password: _authData['password']!,
            );
        FirebaseAnalytics.instance
            .logEvent(name: 'login', parameters: {'success': 'true'});
      } on FirebaseAuthException catch (e) {
        final String message;
        switch (e.code) {
          case "email-already-in-use":
            message = "An account already exists with that email";
          case "email-invalid":
            message = "Email invalis";
          case "weak-password":
            message = "The password most be stronger";
          default:
            message = "The account wasn't found";
        }
        scaffoldMessengerState.showSnackBar(
          ErrorSnackbar(
            error: message,
          ),
        );
        FirebaseAnalytics.instance
            .logEvent(name: 'login', parameters: {'success': 'false'});
      } on NetworkException {
        scaffoldMessengerState.showSnackBar(
          ErrorSnackbar(
            error: "Network error",
          ),
        );
      } catch (e) {
        log.e("An error occurred while updating the photo", error: e);
        scaffoldMessengerState.showSnackBar(
          ErrorSnackbar(
            error: "Unknown error",
          ),
        );
      } finally {
        setState(() {
          _isLoading = false;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: FocusManager.instance.primaryFocus?.unfocus,
        child: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: ExactAssetImage('assets/images/login.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 2.0, sigmaY: 2.0),
            child: Container(
              decoration: BoxDecoration(color: Colors.white.withOpacity(0.0)),
              child: Container(
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Colors.transparent, Colors.black],
                  ),
                ),
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                child: SingleChildScrollView(
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height - 10,
                    child: Form(
                      key: _key,
                      child: Column(
                        children: <Widget>[
                          const Gap(
                            120,
                          ),
                          const Text(
                            'UniFood',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w800,
                              fontSize: 40,
                            ),
                          ),
                          const Spacer(),
                          SizedBox(
                            height: 65,
                            child: TextFormField(
                              cursorColor: Colors.white,
                              style: const TextStyle(color: Colors.white),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return "The field cannot be empty";
                                } else if (!EmailValidator.validate(
                                  value.trim(),
                                )) {
                                  return "Invalid email";
                                }
                                return null;
                              },
                              onSaved: (newValue) {
                                _authData["email"] = newValue!.trim();
                              },
                              decoration: InputDecoration(
                                labelText: 'Email',
                                fillColor: Colors.grey,
                                focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                    color: Colors.white,
                                  ),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                labelStyle:
                                    const TextStyle(color: Colors.white),
                                prefixIcon: const Icon(
                                  Icons.email_outlined,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                          const Gap(20),
                          SizedBox(
                            height: 65,
                            child: TextFormField(
                              cursorColor: Colors.white,
                              style: const TextStyle(color: Colors.white),
                              obscureText: true,
                              onSaved: (newValue) {
                                _authData["password"] = newValue!;
                              },
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return "Empty field";
                                } else if (value.length < 8) {
                                  return "Minimum 8 caracteres";
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                    color: Colors.white,
                                  ),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                labelText: 'Password',
                                fillColor: Colors.grey,
                                labelStyle:
                                    const TextStyle(color: Colors.white),
                                prefixIcon: const Icon(
                                  Icons.lock_outline_sharp,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerRight,
                            child: TextButton(
                              onPressed: () {
                                Navigator.pushNamed(
                                  context,
                                  ForgotPassword.path,
                                );
                              },
                              child: const Text(
                                'Forgot Password?',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                          ),
                          const Gap(
                            50,
                          ),
                          Consumer<AuthProvider>(
                            builder: (context, authProvider, child) {
                              return SizedBox(
                                width: double.infinity,
                                height: 60,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(15),
                                  child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      backgroundColor:
                                          const Color.fromRGBO(60, 75, 248, 1),
                                    ),
                                    onPressed: _isLoading
                                        ? null
                                        : () async {
                                            await _login();
                                          },
                                    child: _isLoading
                                        ? const CircularProgressIndicator
                                            .adaptive()
                                        : const Text(
                                            'Login',
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 18,
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                  ),
                                ),
                              );
                            },
                          ),
                          const Gap(
                            40,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              TextButton(
                                child: const Text(
                                  'Create New Account',
                                  style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.white,
                                    decoration: TextDecoration.underline,
                                    decorationThickness: 0.8,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                                onPressed: () {
                                  Navigator.pushNamed(context, Register.path);
                                },
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
