import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:unifood/domain/model/restaurant/restaurant_model.dart';
import 'package:unifood/domain/model/user/user_model.dart';
import 'package:unifood/presentation/providers/auth_provider.dart';
import 'package:unifood/presentation/providers/restaurant_provider.dart';
import 'package:unifood/presentation/screens/login_screen.dart';
import 'package:unifood/presentation/screens/map_screen.dart';
import 'package:unifood/presentation/screens/profile_screen.dart';
import 'package:unifood/presentation/screens/restaurant_details_screen.dart';

import 'package:unifood/presentation/screens/restaurant_list.dart';

class HomePage extends StatefulWidget {
  static const String path = "/";
  const HomePage({super.key});
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int selectedIndex = 0;

  final List<Widget> bodies = [
    const RestaurantListScreen(),
    const MapScreen(),
    const ProfileDetailScreen(),
  ];

  List<AppBar?> appBars() => [
        AppBar(
          title: const Text("Home"),
          actions: [
            IconButton(
              onPressed: () {
                showSearch(context: context, delegate: MySearchDelegate());
              },
              icon: const Card(
                shape: CircleBorder(),
                elevation: 5,
                child: Padding(
                  padding: EdgeInsets.all(5),
                  child: Icon(Icons.search),
                ),
              ),
            ),
          ],
          leading: IconButton(
            onPressed: () {
              showModalBottomSheet(
                context: context,
                isScrollControlled: true,
                builder: (context) {
                  return Container();
                },
              );
            },
            icon: const Icon(Icons.filter_list),
          ),
        ),
        null,
        AppBar(
          title: const Text("Profile"),
        ),
      ];

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<UserModel?>(
      stream: context.watch<AuthProvider>().userStream,
      builder: (context, snapshot) {
        return snapshot.hasData
            ? Scaffold(
                extendBody: appBars()[selectedIndex] == null,
                bottomNavigationBar: NavigationBar(
                  onDestinationSelected: (index) {
                    setState(() {
                      selectedIndex = index;
                    });
                  },
                  indicatorColor: Colors.transparent,
                  surfaceTintColor: Colors.white,
                  selectedIndex: selectedIndex,
                  destinations: const [
                    NavigationDestination(
                      icon: Icon(Icons.home),
                      label: 'Home',
                    ),
                    NavigationDestination(
                      icon: Icon(Icons.map_rounded),
                      label: 'Map',
                    ),
                    NavigationDestination(
                      icon: Icon(Icons.person),
                      label: 'Profile',
                    ),
                  ],
                ),
                appBar: appBars()[selectedIndex],
                body: bodies[selectedIndex],
              )
            : const Login();
      },
    );
  }
}

class MySearchDelegate extends SearchDelegate {
  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () {
          if (query.isEmpty) {
            close(context, null);
          } else {
            query = '';
          }
        },
        icon: const Icon(Icons.clear),
      ),
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
      onPressed: () {
        close(context, null);
      },
      icon: const Icon(Icons.arrow_back),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Container();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final List<RestaurantModel> suggestions =
        context.read<RestaurantProvider>().restaurants.where((restaurant) {
      final result = restaurant.name.toLowerCase();
      final input = query.toLowerCase();
      return result.contains(input);
    }).toList();
    return ListView.builder(
      itemCount: suggestions.length,
      itemBuilder: (context, index) {
        return ListTile(
          onTap: () {
            query = suggestions[index].name;
            Navigator.of(context).pushNamed(
              RestaurantDetailsScreen.path,
              arguments: suggestions[index],
            );
          },
          title: Text(suggestions[index].name),
          leading: CircleAvatar(
            backgroundImage: CachedNetworkImageProvider(
              suggestions[index].imageName,
            ),
          ),
        );
      },
    );
  }
}
