import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:unifood/presentation/providers/restaurant_provider.dart';
import 'package:unifood/presentation/widgets/map_view.dart';

class MapScreen extends StatelessWidget {
  const MapScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<RestaurantProvider>(
      builder: (context, value, child) =>
          MapView(restaurants: value.restaurants),
    );
  }
}
