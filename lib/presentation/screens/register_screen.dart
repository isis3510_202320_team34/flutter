import 'dart:io';
import 'dart:ui';

import 'package:email_validator/email_validator.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:image_picker/image_picker.dart';
import 'package:logger/logger.dart';
import 'package:provider/provider.dart';
import 'package:unifood/domain/exception/network_exception.dart';
import 'package:unifood/presentation/providers/auth_provider.dart';
import 'package:unifood/presentation/widgets/error_snackbar.dart';
import 'package:unifood/utils/images_options.dart';

class Register extends StatelessWidget {
  static const String path = "/register";
  const Register({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: FocusManager.instance.primaryFocus?.unfocus,
        child: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: ExactAssetImage('assets/images/create_user.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 2.0, sigmaY: 2.0),
            child: Container(
              decoration: BoxDecoration(color: Colors.black.withOpacity(0.5)),
              child: const RegisterContent(),
            ),
          ),
        ),
      ),
    );
  }
}

class RegisterContent extends StatefulWidget {
  const RegisterContent({super.key});

  @override
  State<RegisterContent> createState() => _RegisterContentState();
}

class _RegisterContentState extends State<RegisterContent> {
  bool _isLoading = false;
  final _key = GlobalKey<FormState>();
  final Map<String, String> _authData = {
    'name': '',
    'email': '',
    'password': '',
    'imagePath': '',
  };

  Future<void> handleImageUpload() async {
    final scaffoldMessengerState = ScaffoldMessenger.of(context);
    final log = context.read<Logger>();
    final fromGallery = await showImageOptionsModal(context);
    if (fromGallery == null) {
      return;
    }

    try {
      final imagePicker = ImagePicker();
      final XFile? file = await imagePicker.pickImage(
        source: fromGallery ? ImageSource.gallery : ImageSource.camera,
      );
      if (file == null) {
        return;
      }
      setState(() {
        _authData['imagePath'] = file.path;
      });
    } catch (e) {
      log.e(
        "An error occurred when trying to select a photo on the register screen",
      );
      scaffoldMessengerState.showSnackBar(
        ErrorSnackbar(
          error: e.toString(),
        ),
      );
    }
  }

  Future<void> _submit() async {
    FocusScope.of(context).unfocus();
    final scaffoldMessengerState = ScaffoldMessenger.of(context);
    final navigator = Navigator.of(context);
    final log = context.read<Logger>();
    _key.currentState!.save();
    if (_key.currentState!.validate()) {
      bool success = false;
      try {
        setState(() {
          _isLoading = true;
        });

        await context.read<AuthProvider>().createUserWithEmailAndPassword(
              email: _authData['email']!.trim(),
              password: _authData['password']!,
              name: _authData['name']!,
              imagePath: _authData['imagePath']!,
            );
        FirebaseAnalytics.instance
            .logEvent(name: 'sign_up', parameters: {'success': 'true'});
        success = true;
      } on FirebaseAuthException catch (e) {
        final String message;
        switch (e.code) {
          case "email-already-in-use":
            message = "An account already exists with that email";
          case "email-invalid":
            message = "Email invalis";
          case "weak-password":
            message = "The password most be stronger";
          default:
            message = "Not account found";
        }
        scaffoldMessengerState.showSnackBar(
          ErrorSnackbar(
            error: message,
          ),
        );
        FirebaseAnalytics.instance
            .logEvent(name: 'sign_up', parameters: {'success': 'false'});
      } on NetworkException {
        scaffoldMessengerState.showSnackBar(
          ErrorSnackbar(
            error: "Network error",
          ),
        );
      } catch (e) {
        log.e("An error occurred on the register screen", error: e);
        scaffoldMessengerState.showSnackBar(
          ErrorSnackbar(
            error: "Unknown error",
          ),
        );
      } finally {
        setState(() {
          _isLoading = false;
        });
        if (success) {
          navigator.pop();
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [Colors.transparent, Colors.black],
        ),
      ),
      child: Form(
        key: _key,
        child: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 30),
          child: SizedBox(
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: <Widget>[
                const SizedBox(
                  height: 80,
                ),
                Stack(
                  children: [
                    Positioned(
                      child: CircleAvatar(
                        backgroundColor:
                            const Color.fromRGBO(120, 120, 120, 0.9),
                        radius: 80,
                        backgroundImage: FileImage(
                          File(_authData["imagePath"]!),
                        ),
                        child: Visibility(
                          visible: _authData["imagePath"]! == '',
                          child: const Icon(
                            Icons.person_4_outlined,
                            color: Colors.white,
                            size: 70,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      right: 0,
                      bottom: 0,
                      child: InkWell(
                        onTap: handleImageUpload,
                        child: Container(
                          height: 50,
                          width: 50,
                          decoration: BoxDecoration(
                            color: const Color.fromRGBO(60, 75, 248, 1.0),
                            border: Border.all(color: Colors.white, width: 1.5),
                            shape: BoxShape.circle,
                          ),
                          child: const Icon(
                            Icons.arrow_upward,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                const Spacer(),
                SizedBox(
                  height: 65,
                  child: TextFormField(
                    cursorColor: Colors.white,
                    onSaved: (newValue) {
                      _authData["name"] = newValue!;
                    },
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "The field cannot be empty";
                      }
                      return null;
                    },
                    style: const TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(
                          color: Colors.white,
                        ),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      labelText: 'Name',
                      labelStyle: const TextStyle(color: Colors.white),
                      prefixIcon: const Icon(
                        Icons.person_2_outlined,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                const Gap(20),
                SizedBox(
                  height: 65,
                  child: TextFormField(
                    cursorColor: Colors.white,
                    style: const TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "The field cannot be empty";
                      } else if (!EmailValidator.validate(value.trim())) {
                        return "Invalid email";
                      }
                      return null;
                    },
                    onSaved: (newValue) {
                      _authData["email"] = newValue!.trim();
                    },
                    decoration: InputDecoration(
                      labelText: 'Email',
                      labelStyle: const TextStyle(color: Colors.white),
                      prefixIcon: const Icon(
                        Icons.email_outlined,
                        color: Colors.white,
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(
                          color: Colors.white,
                        ),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                SizedBox(
                  height: 65,
                  child: TextFormField(
                    cursorColor: Colors.white,
                    style: const TextStyle(color: Colors.white),
                    onSaved: (newValue) {
                      _authData["password"] = newValue!;
                    },
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Empty field";
                      } else if (value.length < 8) {
                        return "Minimum 8 caracteres";
                      }
                      return null;
                    },
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: 'Password',
                      labelStyle: const TextStyle(color: Colors.white),
                      prefixIcon: const Icon(
                        Icons.lock_outline_sharp,
                        color: Colors.white,
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(
                          color: Colors.white,
                        ),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                SizedBox(
                  height: 65,
                  child: TextFormField(
                    cursorColor: Colors.white,
                    style: const TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value != _authData['password']) {
                        return 'The password does not match';
                      }
                      return null;
                    },
                    obscureText: true,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(
                          color: Colors.white,
                        ),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      labelText: 'Confirm Password',
                      labelStyle: const TextStyle(color: Colors.white),
                      prefixIcon: const Icon(
                        Icons.lock_outline_sharp,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 50,
                ),
                Consumer<AuthProvider>(
                  builder: (context, authProvider, child) {
                    return SizedBox(
                      width: double.infinity,
                      height: 60,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(15),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor:
                                const Color.fromRGBO(60, 75, 248, 1.0),
                          ),
                          onPressed: _isLoading
                              ? null
                              : () async {
                                  await _submit();
                                },
                          child: _isLoading
                              ? const CircularProgressIndicator()
                              : const Text(
                                  'Register',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                        ),
                      ),
                    );
                  },
                ),
                const SizedBox(
                  height: 40,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const Text(
                      'Already have an account?',
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                      ),
                    ),
                    TextButton(
                      child: const Text(
                        'Login',
                        style: TextStyle(
                          fontSize: 18,
                          color: Color.fromRGBO(60, 75, 248, 1),
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
