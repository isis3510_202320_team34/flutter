import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:gap/gap.dart';
import 'package:image_picker/image_picker.dart';
import 'package:logger/logger.dart';
import 'package:provider/provider.dart';
import 'package:unifood/domain/exception/network_exception.dart';
import 'package:unifood/domain/exception/prohibit_content_exception.dart';
import 'package:unifood/domain/model/restaurant/restaurant_model.dart';
import 'package:unifood/presentation/providers/review_provider.dart';
import 'package:unifood/presentation/widgets/error_snackbar.dart';
import 'package:unifood/presentation/widgets/horizontal_images.dart';

class AddReviewScreen extends StatefulWidget {
  final RestaurantModel restaurant;
  const AddReviewScreen({super.key, required this.restaurant});

  @override
  State<AddReviewScreen> createState() => _AddReviewScreenState();
}

class _AddReviewScreenState extends State<AddReviewScreen> {
  double _rate = 5.0;
  bool _isLoading = false;
  late final TextEditingController _reviewController;
  List<String> _imagesPath = [];
  String error = "";

  @override
  void dispose() {
    _reviewController.dispose();
    super.dispose();
  }

  Future<void> handleImagesUpload() async {
    final imagePicker = ImagePicker();
    final files = await imagePicker.pickMultiImage(
      maxWidth: 600,
      maxHeight: 600,
      imageQuality: 50,
    );
    if (files.isEmpty) {
      return;
    }
    setState(() {
      _imagesPath = files.map((file) => file.path).toList();
    });
  }

  void removeHandler(int idx) {
    setState(() {
      _imagesPath.removeAt(idx);
    });
  }

  void uploadHandler() {
    FocusScope.of(context).unfocus();
    final scaffoldMessengerState = ScaffoldMessenger.of(context);
    final nav = Navigator.of(context);
    final log = context.read<Logger>();
    if (_reviewController.text.isEmpty) {
      setState(() {
        error = "There needs to be some content";
      });
      return;
    }
    try {
      setState(() {
        _isLoading = true;
      });

      context.read<ReviewProvider>().addReview(
            content: _reviewController.text,
            rate: _rate,
            imagesPath: _imagesPath,
            restaurantId: widget.restaurant.id,
          );
      FirebaseAnalytics.instance
          .logEvent(name: 'review_creation', parameters: {'success': 'true'});
    } on NetworkException {
      scaffoldMessengerState.showSnackBar(
        ErrorSnackbar(
          error: "Network error",
        ),
      );
    } on ProhibitContentException {
      scaffoldMessengerState.showSnackBar(
        ErrorSnackbar(
          error: "Explicit content is prohibited on this app!",
        ),
      );
    } catch (e) {
      log.e("An error occurred while updating a review", error: e);
      scaffoldMessengerState.showSnackBar(
        ErrorSnackbar(
          error: "Unknown error",
        ),
      );
    } finally {
      setState(() {
        _isLoading = false;
      });
      nav.pop<bool>(true);
    }
  }

  @override
  void initState() {
    super.initState();
    _reviewController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * .9,
      child: GestureDetector(
        onTap: FocusManager.instance.primaryFocus?.unfocus,
        child: Container(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Row(
                children: [
                  const Spacer(),
                  TextButton(
                    onPressed: () async {
                      uploadHandler();
                    },
                    child: _isLoading
                        ? const CircularProgressIndicator.adaptive()
                        : const Text("Add"),
                  ),
                ],
              ),
              const Center(
                child: Text(
                  "Add a Review",
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 24,
                  ),
                ),
              ),
              const Gap(16.0),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Center(
                  child: RatingBar.builder(
                    initialRating: _rate,
                    minRating: 1,
                    allowHalfRating: true,
                    itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
                    itemBuilder: (context, _) => const Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (rating) {
                      setState(() {
                        _rate = rating;
                      });
                    },
                  ),
                ),
              ),
              const Gap(16.0),
              const Center(
                child: Text(
                  "Write your review:",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF8A98BA),
                  ),
                ),
              ),
              const Gap(8.0),
              TextField(
                controller: _reviewController,
                maxLines: 4,
                decoration: const InputDecoration(
                  contentPadding: EdgeInsets.all(8),
                  fillColor: Color.fromARGB(255, 228, 222, 222),
                ),
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              const Gap(
                20,
              ),
              TextButton.icon(
                style: const ButtonStyle(alignment: Alignment.centerLeft),
                onPressed: handleImagesUpload,
                icon: const Icon(Icons.photo),
                label: const Text("Add photo"),
              ),
              const Gap(
                20,
              ),
              HorizontalFileImagesList(
                imagePaths: _imagesPath,
                removeHandler: removeHandler,
              ),
              Visibility(
                visible: error.isNotEmpty,
                child: Center(
                  child: Text(
                    error,
                    style: const TextStyle(
                      color: Colors.red,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
