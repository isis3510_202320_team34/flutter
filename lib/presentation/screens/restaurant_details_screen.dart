import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:provider/provider.dart';
import 'package:unifood/domain/model/restaurant/restaurant_model.dart';
import 'package:unifood/presentation/providers/review_provider.dart';
import 'package:unifood/presentation/screens/add_review.dart';
import 'package:unifood/presentation/widgets/restaurant_sliver_content.dart';
import 'package:url_launcher/url_launcher.dart';

class RestaurantDetailsScreen extends StatelessWidget {
  static const String path = "/restaurant-details";
  final RestaurantModel restaurant;
  const RestaurantDetailsScreen({super.key, required this.restaurant});

  Future<void> openGoogleMapsForDirections(double lat, double lng) async {
    final String googleMapsUrl = 'https://www.google.com/maps?q=$lat,$lng';
    if (await canLaunchUrl(Uri.parse(googleMapsUrl))) {
      await launchUrl(Uri.parse(googleMapsUrl));
    } else {
      throw 'Could not launch Google Maps';
    }
  }

  Future<void> openPhoneDialer(String phoneNumber) async {
    final String phoneUrl = 'tel:$phoneNumber';
    if (await canLaunchUrl(Uri.parse(phoneUrl))) {
      await launchUrl(Uri.parse(phoneUrl));
    } else {
      throw 'Could not launch the phone dialer';
    }
  }

  Future<void> addReviewHandler(BuildContext ctx) async {
    FirebaseAnalytics.instance
        .logEvent(name: 'review_creation', parameters: {'success': 'true'});
    await showModalBottomSheet<bool>(
      isScrollControlled: true,
      context: ctx,
      builder: (context) {
        return AddReviewScreen(
          restaurant: restaurant,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Consumer<ReviewProvider>(
        builder: (context, restaurantProvider, child) {
          return FutureBuilder(
            future: restaurantProvider.canPost(restaurant.id),
            builder: (ctx, snapshot) => Visibility(
              visible: snapshot.data ?? false,
              child: FloatingActionButton(
                onPressed: () {
                  addReviewHandler(context);
                },
                backgroundColor: const Color.fromRGBO(60, 75, 248, 1.0),
                child: const Icon(
                  Icons.star,
                  color: Colors.white,
                ),
              ),
            ),
          );
        },
      ),
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            expandedHeight: 200,
            flexibleSpace: Stack(
              children: [
                Positioned.fill(
                  child: CachedNetworkImage(
                    imageUrl: restaurant.imageName,
                    progressIndicatorBuilder:
                        (context, url, downloadProgress) =>
                            const CircularProgressIndicator.adaptive(),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                  bottom: 20,
                  left: 50,
                  right: 50,
                  child: ClipRRect(
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
                      child: Container(
                        height: 50,
                        width: 120,
                        decoration: BoxDecoration(
                          color: Colors.black.withOpacity(0.3),
                          borderRadius: BorderRadius.circular(100),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            InkWell(
                              onTap: () {
                                openPhoneDialer(restaurant.phone);
                              },
                              child: Row(
                                children: [
                                  ClipOval(
                                    child: Container(
                                      padding: const EdgeInsets.all(4),
                                      color: Colors.white,
                                      child: const Icon(
                                        Icons.phone,
                                        color: Colors.blue,
                                        size: 18,
                                      ),
                                    ),
                                  ),
                                  const Gap(10),
                                  Text(
                                    "Telefono",
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleSmall!
                                        .apply(
                                          color: Colors.white,
                                        ),
                                  ),
                                ],
                              ),
                            ),
                            VerticalDivider(
                              color: Colors.white.withOpacity(0.4),
                              indent: 10,
                              endIndent: 10,
                              width: 30,
                            ),
                            InkWell(
                              onTap: () {
                                openGoogleMapsForDirections(
                                  restaurant.latitude,
                                  restaurant.longitude,
                                );
                              },
                              child: Row(
                                children: [
                                  ClipOval(
                                    child: Container(
                                      padding: const EdgeInsets.all(4),
                                      color: Colors.white,
                                      child: const Icon(
                                        Icons.directions,
                                        color: Colors.blue,
                                        size: 18,
                                      ),
                                    ),
                                  ),
                                  const Gap(10),
                                  Text(
                                    "Direccion",
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleSmall!
                                        .apply(
                                          color: Colors.white,
                                        ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SliverContent(restaurant: restaurant),
        ],
      ),
    );
  }
}
