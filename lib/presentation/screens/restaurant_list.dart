import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:unifood/domain/exception/network_exception.dart';
import 'package:unifood/presentation/providers/restaurant_provider.dart';
import 'package:unifood/presentation/widgets/error_snackbar.dart';
import 'package:unifood/presentation/widgets/restaurant_item.dart';
import 'package:unifood/utils/location.dart';

class RestaurantListScreen extends StatelessWidget {
  static const String path = "/restaurants";
  const RestaurantListScreen({super.key});

  Future<void> _fetchRestaurants(BuildContext context) async {
    final scaffoldMessengerState = ScaffoldMessenger.of(context);
    final restaurantProvider = context.read<RestaurantProvider>();
    try {
      await Location.init();
      if (restaurantProvider.restaurants.isEmpty) {
        await restaurantProvider.listRestaurants();
      }
    } on NetworkException {
      scaffoldMessengerState
          .showSnackBar(ErrorSnackbar(error: "Network error"));
    } catch (e) {
      scaffoldMessengerState
          .showSnackBar(ErrorSnackbar(error: "Unknown error"));
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _fetchRestaurants(context),
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.done:
            return Consumer<RestaurantProvider>(
              builder: (context, value, child) {
                return RefreshIndicator.adaptive(
                  onRefresh: () async {
                    await value.updateRestaurants();
                  },
                  child: ListView.builder(
                    itemCount: value.restaurants.length,
                    itemBuilder: (ctx, idx) {
                      return Column(
                        children: [
                          Visibility(
                            visible: idx == 0,
                            child: const Divider(
                              color: Colors.transparent,
                              height: 25,
                            ),
                          ),
                          RestaurantItem(
                            restaurant: value.restaurants[idx],
                          ),
                          const Divider(
                            color: Colors.transparent,
                            height: 15,
                          ),
                        ],
                      );
                    },
                  ),
                );
              },
            );
          default:
            return const Center(
              child: CircularProgressIndicator(),
            );
        }
      },
    );
  }
}
