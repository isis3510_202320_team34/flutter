import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:logger/logger.dart';
import 'package:provider/provider.dart';
import 'package:unifood/domain/exception/network_exception.dart';
import 'package:unifood/domain/model/user/user_model.dart';
import 'package:unifood/presentation/providers/auth_provider.dart';
import 'package:unifood/presentation/widgets/error_snackbar.dart';
import 'package:unifood/utils/images_options.dart';

class ProfileDetailScreen extends StatefulWidget {
  static const String path = "/profile";
  const ProfileDetailScreen({super.key});

  @override
  State<ProfileDetailScreen> createState() => _ProfileDetailScreenState();
}

class _ProfileDetailScreenState extends State<ProfileDetailScreen> {
  bool _isLoading = false;

  Future<void> _handleUploadImage() async {
    final scaffoldMessengerState = ScaffoldMessenger.of(context);
    final authProvider = context.read<AuthProvider>();
    final log = context.read<Logger>();
    try {
      final fromGallery = await showImageOptionsModal(context);
      if (fromGallery == null) {
        return;
      }
      setState(() {
        _isLoading = true;
      });
      final imagePicker = ImagePicker();
      final XFile? file = await imagePicker.pickImage(
        source: fromGallery ? ImageSource.gallery : ImageSource.camera,
      );
      if (file == null) {
        return;
      }
      await authProvider.uploadImage(file.path);
    } on NetworkException {
      scaffoldMessengerState.showSnackBar(
        ErrorSnackbar(
          error: "Network error",
        ),
      );
    } catch (e) {
      log.e("An error occurred while updating the photo", error: e);
      scaffoldMessengerState.showSnackBar(
        ErrorSnackbar(
          error: "Unknown error",
        ),
      );
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<UserModel?>(
      stream: context.watch<AuthProvider>().userStream,
      builder: (context, snapshot) {
        return snapshot.hasData
            ? SingleChildScrollView(
                child: Column(
                  children: [
                    if (_isLoading)
                      const CircleAvatar(
                        backgroundColor: Color.fromRGBO(120, 120, 120, 0.9),
                        radius: 80,
                        child: CircularProgressIndicator.adaptive(),
                      )
                    else
                      Consumer<AuthProvider>(
                        builder: (context, authProvider, child) {
                          return FutureBuilder<File>(
                            future: authProvider.userProfileImageFile,
                            builder: (context, snapshot2) {
                              switch (snapshot2.connectionState) {
                                case ConnectionState.done:
                                  return InkWell(
                                    onTap: _handleUploadImage,
                                    child: CircleAvatar(
                                      backgroundColor: Colors.grey,
                                      radius: 80,
                                      backgroundImage: FileImage(
                                        snapshot2.data!,
                                      ),
                                    ),
                                  );
                                default:
                                  return Container();
                              }
                            },
                          );
                        },
                      ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16),
                      child: Text(
                        snapshot.data!.email,
                        style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    const Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Column(
                          children: [
                            Text(
                              '12',
                              style: TextStyle(
                                fontSize: 24,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text('Reviews'),
                          ],
                        ),
                        Column(
                          children: [
                            Text(
                              '6',
                              style: TextStyle(
                                fontSize: 24,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text('Reservations'),
                          ],
                        ),
                      ],
                    ),
                    const Divider(
                      color: Colors.grey,
                      thickness: 1,
                      height: 32,
                      indent: 16,
                      endIndent: 16,
                    ),
                    const ProfileItem(title: 'Privacy'),
                    const ProfileItem(title: 'History of Reservations'),
                    const ProfileItem(title: 'Reviews'),
                    const ProfileItem(title: 'Payment Methods'),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20),
                      child: ElevatedButton(
                        onPressed: () async {
                          await context.read<AuthProvider>().signOut();
                        },
                        style: ElevatedButton.styleFrom(
                          foregroundColor: Colors.red,
                        ),
                        child: const Text('Log Out'),
                      ),
                    ),
                  ],
                ),
              )
            : const Center(
                child: CircularProgressIndicator.adaptive(),
              );
      },
    );
  }
}

class ProfileItem extends StatelessWidget {
  final String title;

  const ProfileItem({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(title),
      onTap: () {
        // Handle item tap action
      },
    );
  }
}
