import 'package:flutter/material.dart';

class NoInternet extends StatelessWidget {
  const NoInternet({super.key});

  @override
  Widget build(BuildContext context) {
    return Expanded(child: Image.asset('assets/images/no_internet.png'));
  }
}
