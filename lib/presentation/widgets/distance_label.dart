import 'package:flutter/material.dart';
import 'package:unifood/domain/model/restaurant/restaurant_model.dart';

class DistanceLabel extends StatelessWidget {
  const DistanceLabel({
    super.key,
    required this.restaurant,
  });

  final RestaurantModel restaurant;

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: restaurant.distance != -1,
      child: Container(
        decoration: BoxDecoration(
          color: const Color.fromRGBO(132, 141, 255, 1),
          borderRadius: BorderRadius.circular(20),
        ),
        padding: const EdgeInsets.symmetric(vertical: 1, horizontal: 5),
        child: Text(
          "${restaurant.distance.toStringAsFixed(1)} Km",
          style: Theme.of(context)
              .textTheme
              .titleSmall!
              .apply(color: Colors.white, fontSizeDelta: -3),
        ),
      ),
    );
  }
}
