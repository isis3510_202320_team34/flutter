import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:provider/provider.dart';
import 'package:unifood/domain/exception/network_exception.dart';
import 'package:unifood/domain/model/restaurant/restaurant_model.dart';
import 'package:unifood/domain/model/review/review_model.dart';
import 'package:unifood/presentation/providers/review_provider.dart';
import 'package:unifood/presentation/widgets/error_snackbar.dart';
import 'package:unifood/presentation/widgets/star_raiting_label.dart';

class ReviewsList extends StatelessWidget {
  final RestaurantModel restaurant;
  const ReviewsList({super.key, required this.restaurant});

  Future<void> fetchReviews(BuildContext context) async {
    final scaffoldMessengerState = ScaffoldMessenger.of(context);
    final log = context.read<Logger>();
    final reviewsProvider = context.read<ReviewProvider>();
    try {
      if (reviewsProvider.restaurantReviews(restaurant.id) == null) {
        await reviewsProvider.listRestaurantReviews(restaurant.id);
      }
    } on NetworkException {
      scaffoldMessengerState.showSnackBar(
        ErrorSnackbar(
          error: "Network error",
        ),
      );
    } catch (e) {
      log.e("An error occurred while updating a review", error: e);
      scaffoldMessengerState.showSnackBar(
        ErrorSnackbar(
          error: "Unknown error",
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: fetchReviews(context),
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.done:
            return Consumer<ReviewProvider>(
              builder: (context, reviewProvider, child) {
                return reviewProvider.restaurantReviews(restaurant.id) !=
                            null &&
                        reviewProvider
                            .restaurantReviews(restaurant.id)!
                            .isNotEmpty
                    ? Column(
                        children: reviewProvider
                            .restaurantReviews(restaurant.id)!
                            .map(
                          (review) {
                            return ReviewListTile(review);
                          },
                        ).toList(),
                      )
                    : const Center(
                        child: Text(
                          "There are no reviews",
                          style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      );
              },
            );

          default:
            return const Center(
              child: CircularProgressIndicator.adaptive(),
            );
        }
      },
    );
  }
}

class ReviewListTile extends StatefulWidget {
  final ReviewModel review;
  const ReviewListTile(this.review, {super.key});

  @override
  State<ReviewListTile> createState() => _ReviewListTileState();
}

class _ReviewListTileState extends State<ReviewListTile> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: context.read<ReviewProvider>().getUserByReview(widget.review),
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.done:
            return ListTile(
              leading: CircleAvatar(
                radius: 25,
                backgroundImage: snapshot.data?.imageUrl != null
                    ? NetworkImage(snapshot.data!.imageUrl)
                    : null,
              ),
              title: snapshot.data != null
                  ? Text(snapshot.data!.name)
                  : const Text("Unknown"),
              subtitle: Text(widget.review.content),
              trailing: StartRaitingLabel(rate: widget.review.rate),
            );
          default:
            return const Center(
              child: CircularProgressIndicator.adaptive(),
            );
        }
      },
    );
  }
}
