import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:unifood/domain/model/restaurant/restaurant_model.dart';
import 'package:unifood/presentation/widgets/category_label.dart';
import 'package:unifood/presentation/widgets/distance_label.dart';
import 'package:unifood/presentation/widgets/horizontal_images.dart';
import 'package:unifood/presentation/widgets/is_open_label.dart';
import 'package:unifood/presentation/widgets/map_view.dart';
import 'package:unifood/presentation/widgets/restaurant_reviews_list.dart';
import 'package:unifood/presentation/widgets/star_raiting_label.dart';

class SliverContent extends StatelessWidget {
  const SliverContent({
    super.key,
    required this.restaurant,
  });

  final RestaurantModel restaurant;

  @override
  Widget build(BuildContext context) {
    return SliverPadding(
      padding: const EdgeInsets.all(20),
      sliver: SliverList(
        delegate: SliverChildListDelegate(
          [
            Text(
              restaurant.name,
              style: Theme.of(context).textTheme.headlineSmall!.apply(
                    fontWeightDelta: 2,
                  ),
            ),
            Row(
              children: [
                const Gap(10),
                CategoryLabel(category: restaurant.category),
                const SizedBox(width: 6),
                DistanceLabel(restaurant: restaurant),
                const Spacer(),
                StartRaitingLabel(rate: restaurant.rate),
              ],
            ),
            const Gap(10),
            Text(
              restaurant.address,
              style: Theme.of(context)
                  .textTheme
                  .labelLarge!
                  .apply(color: Colors.grey[600]),
            ),
            const Gap(5),
            Row(
              children: [
                IsOpenLabel(isOpen: restaurant.isOpen),
                Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                        text: "daily time ",
                        style: Theme.of(context)
                            .textTheme
                            .labelMedium!
                            .apply(color: Colors.grey[600]),
                      ),
                      TextSpan(
                        text: "9:30 a.m. to 11:00 p.m.",
                        style: Theme.of(context)
                            .textTheme
                            .labelLarge!
                            .apply(color: const Color.fromRGBO(255, 96, 87, 1)),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.grey, width: 2),
                borderRadius: BorderRadius.circular(10),
              ),
              height: MediaQuery.of(context).size.width * .65,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: MapView(
                  gestures: false,
                  navigateOnTap: false,
                  initialPosition:
                      LatLng(restaurant.latitude, restaurant.longitude),
                  restaurants: [restaurant],
                ),
              ),
            ),
            const Gap(20),
            Row(
              children: [
                Text(
                  "Menu y Photos",
                  style: Theme.of(context)
                      .textTheme
                      .headlineSmall!
                      .apply(color: Colors.grey[800]),
                ),
                const Spacer(),
                TextButton(onPressed: () {}, child: const Text("See all (32)")),
              ],
            ),
            const HorizontalNetworkImagesList(
              imagePaths: [
                "https://anestisxasapotaverna.gr/wp-content/uploads/2021/12/ARTICLE-1.jpg",
              ],
            ),
            const Gap(20),
            Row(
              children: [
                Text(
                  "Reviews",
                  style: Theme.of(context)
                      .textTheme
                      .headlineSmall!
                      .apply(color: Colors.grey[800]),
                ),
                const Spacer(),
                TextButton(onPressed: () {}, child: const Text("See all (32)")),
              ],
            ),
            ReviewsList(
              restaurant: restaurant,
            ),
            const Gap(20),
          ],
        ),
      ),
    );
  }
}
