import 'dart:async';
import 'package:flutter/material.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:unifood/domain/model/restaurant/restaurant_model.dart';
import 'package:unifood/presentation/screens/restaurant_details_screen.dart';

class MapView extends StatefulWidget {
  final LatLng initialPosition;
  final List<RestaurantModel> restaurants;
  final bool gestures;
  final bool navigateOnTap;
  const MapView({
    super.key,
    this.restaurants = const [],
    this.initialPosition = const LatLng(4.603125, -74.064749),
    this.gestures = true,
    this.navigateOnTap = true,
  });
  @override
  State<MapView> createState() => MapViewState();
}

class MapViewState extends State<MapView> {
  String _mapTheme = "";

  final Completer<GoogleMapController> _controller = Completer();

  late final CameraPosition _kGooglePlex;

  late final Set<Marker> _markers;

  @override
  void initState() {
    super.initState();

    _kGooglePlex = CameraPosition(
      target: widget.initialPosition,
      zoom: 17,
    );
    DefaultAssetBundle.of(context)
        .loadString("lib/theme/map_theme.json")
        .then((value) {
      _mapTheme = value;
    });
    _markers = <Marker>{};

    for (final restaurant in widget.restaurants) {
      _markers.add(
        Marker(
          markerId: MarkerId(restaurant.id),
          onTap: widget.navigateOnTap
              ? () => Navigator.pushNamed(
                    context,
                    RestaurantDetailsScreen.path,
                    arguments: restaurant,
                  )
              : null,
          position: LatLng(
            restaurant.latitude,
            restaurant.longitude,
          ),
          infoWindow: InfoWindow(title: restaurant.name),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return GoogleMap(
      initialCameraPosition: _kGooglePlex,
      myLocationEnabled: true,
      indoorViewEnabled: true,
      mapToolbarEnabled: false,
      myLocationButtonEnabled: widget.gestures,
      rotateGesturesEnabled: widget.gestures,
      scrollGesturesEnabled: widget.gestures,
      tiltGesturesEnabled: widget.gestures,
      zoomControlsEnabled: widget.gestures,
      zoomGesturesEnabled: widget.gestures,
      onMapCreated: (GoogleMapController controller) {
        controller.setMapStyle(_mapTheme);
        _controller.complete(controller);
      },
      markers: _markers,
    );
  }
}
