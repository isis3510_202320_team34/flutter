import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:unifood/domain/model/restaurant/restaurant_model.dart';
import 'package:unifood/presentation/screens/restaurant_details_screen.dart';
import 'package:unifood/presentation/widgets/category_label.dart';
import 'package:unifood/presentation/widgets/distance_label.dart';
import 'package:unifood/presentation/widgets/is_open_label.dart';
import 'package:unifood/presentation/widgets/star_raiting_label.dart';

class RestaurantItem extends StatelessWidget {
  final RestaurantModel restaurant;
  late final int textSize;
  RestaurantItem({
    super.key,
    required this.restaurant,
  }) {
    textSize = restaurant.name.length + restaurant.category.length;
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(
          context,
          RestaurantDetailsScreen.path,
          arguments: restaurant,
        );
      },
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Center(
              child: Stack(
                children: [
                  SizedBox(
                    width: double.infinity,
                    child: ClipRRect(
                      borderRadius: const BorderRadius.vertical(
                        top: Radius.circular(10),
                      ),
                      child: CachedNetworkImage(
                        imageUrl: restaurant.imageName,
                        progressIndicatorBuilder:
                            (context, url, downloadProgress) => const Center(
                          child: CircularProgressIndicator.adaptive(),
                        ),
                        errorWidget: (context, url, error) =>
                            const Icon(Icons.error),
                        fit: BoxFit.cover,
                        height: 250,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 10,
                    left: 10,
                    child: IsOpenLabel(isOpen: restaurant.isOpen),
                  ),
                  Positioned(
                    top: 10,
                    right: 10,
                    child: StartRaitingLabel(rate: restaurant.rate),
                  ),
                ],
              ),
            ),
            ListTile(
              title: Row(
                children: [
                  Text(
                    restaurant.name.length < 14
                        ? restaurant.name
                        : "${restaurant.name.substring(0, 14)}...",
                    style: Theme.of(context)
                        .textTheme
                        .titleLarge!
                        .apply(fontWeightDelta: 10),
                    overflow: TextOverflow.ellipsis,
                  ),
                  const Gap(10),
                  CategoryLabel(category: restaurant.category),
                  const Gap(10),
                  DistanceLabel(restaurant: restaurant),
                  const Spacer(),
                ],
              ),
              subtitle: Text(
                restaurant.address,
                style: const TextStyle(color: Colors.black54),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
