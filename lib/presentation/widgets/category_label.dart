import 'package:flutter/material.dart';

class CategoryLabel extends StatelessWidget {
  const CategoryLabel({
    super.key,
    required this.category,
  });

  final String category;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: const LinearGradient(
          begin: Alignment.topLeft, // Punto de inicio del gradiente
          end: Alignment.bottomRight, // Punto de fin del gradiente
          colors: [
            Colors.yellow, // Color inicial
            Colors.red, // Color final
          ],
        ),
        borderRadius: BorderRadius.circular(20),
      ),
      padding: const EdgeInsets.symmetric(vertical: 1, horizontal: 5),
      child: Text(
        category.length < 10 ? category : "${category.substring(0, 10)}...",
        style: Theme.of(context)
            .textTheme
            .titleSmall!
            .apply(color: Colors.white, fontSizeDelta: -3),
      ),
    );
  }
}
