import 'package:flutter/material.dart';

class SuccessSnackbar extends SnackBar {
  final String? success;
  SuccessSnackbar({this.success, super.key})
      : super(
          content: Column(
            children: [
              const Text('success!'),
              if (success != null) Text(success),
            ],
          ),
          backgroundColor: Colors.green[400],
        );
}
