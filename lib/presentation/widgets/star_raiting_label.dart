import 'package:flutter/material.dart';

class StartRaitingLabel extends StatelessWidget {
  const StartRaitingLabel({
    super.key,
    required this.rate,
  });

  final double rate;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 60,
      decoration: BoxDecoration(
        color: Colors.grey[200],
        borderRadius: BorderRadius.circular(5),
      ),
      padding: const EdgeInsets.symmetric(vertical: 2),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          const Icon(
            Icons.star,
            color: Colors.amber,
          ),
          Text(
            rate.toString(),
            style: Theme.of(context).textTheme.titleSmall!.apply(
                  color: Colors.black54,
                  fontWeightDelta: 9,
                ),
          ),
        ],
      ),
    );
  }
}
