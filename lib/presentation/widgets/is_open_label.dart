import 'package:flutter/material.dart';

class IsOpenLabel extends StatelessWidget {
  const IsOpenLabel({
    super.key,
    required this.isOpen,
  });

  final bool isOpen;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
      ),
      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
      child: Text(
        isOpen ? "OPEN" : "CLOSED",
        style: Theme.of(context).textTheme.titleSmall!.apply(
              color: isOpen ? Colors.green : Colors.red,
              fontWeightDelta: 9,
            ),
      ),
    );
  }
}
