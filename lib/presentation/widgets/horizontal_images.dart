import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class HorizontalNetworkImagesList extends StatelessWidget {
  final List<String> imagePaths;
  const HorizontalNetworkImagesList({
    super.key,
    required this.imagePaths,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 150,
      child: ListView.builder(
        itemCount: imagePaths.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (ctx, idx) {
          return Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: CachedNetworkImage(
                  imageUrl: imagePaths[idx],
                  fit: BoxFit.cover,
                ),
              ),
              const VerticalDivider(
                color: Colors.transparent,
                width: 20,
              ),
            ],
          );
        },
      ),
    );
  }
}

class HorizontalFileImagesList extends StatelessWidget {
  final List<String> imagePaths;
  final void Function(int)? removeHandler;
  const HorizontalFileImagesList({
    super.key,
    required this.imagePaths,
    this.removeHandler,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 150,
      child: ListView.builder(
        itemCount: imagePaths.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (ctx, idx) {
          return Row(
            children: [
              Stack(
                children: [
                  Positioned(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.file(
                        File(imagePaths[idx]),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  if (removeHandler != null)
                    Positioned(
                      top: 5,
                      right: 5,
                      child: InkWell(
                        onTap: () {
                          removeHandler!(idx);
                        },
                        child: Container(
                          height: 20,
                          width: 20,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color: Colors.red,
                          ),
                          child: const Icon(
                            Icons.remove,
                            color: Colors.white,
                            size: 15,
                          ),
                        ),
                      ),
                    ),
                ],
              ),
              const VerticalDivider(
                color: Colors.transparent,
                width: 20,
              ),
            ],
          );
        },
      ),
    );
  }
}
