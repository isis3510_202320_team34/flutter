import 'package:flutter/material.dart';

class CategoryChips extends StatefulWidget {
  final Set<String> categories;
  final void Function(Map<String, bool>) callback;
  const CategoryChips({
    super.key,
    required this.categories,
    required this.callback,
  });

  @override
  State<CategoryChips> createState() => CategoryChipsState();
}

class CategoryChipsState extends State<CategoryChips> {
  late Map<String, bool> _selectedCategories;

  @override
  void initState() {
    super.initState();
    _selectedCategories = {};
    for (final category in widget.categories) {
      _selectedCategories[category] = true;
    }
    widget.callback(_selectedCategories);
  }

  Map<String, bool> get selectedCategories => _selectedCategories;

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        ...widget.categories.map((category) {
          return Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              FilterChip(
                elevation: 5,
                label: Text(category),
                selected: _selectedCategories[category]!,
                onSelected: (isSelected) {
                  setState(() {
                    _selectedCategories[category] = isSelected;
                  });
                },
              ),
              const VerticalDivider(),
            ],
          );
        }),
      ],
    );
  }
}
