import 'package:flutter/material.dart';

class ErrorSnackbar extends SnackBar {
  final String? error;
  ErrorSnackbar({
    this.error,
    super.duration = const Duration(seconds: 3),
    super.key,
  }) : super(
          content: Column(
            children: [
              const Text('An error occurred!'),
              if (error != null) Text(error),
            ],
          ),
          backgroundColor: Colors.red,
        );
}
