import 'dart:async';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';
import 'package:unifood/constants/index.dart';
import 'package:unifood/data/respository/user_repository.dart';
import 'package:unifood/domain/exception/network_exception.dart';
import 'package:unifood/domain/model/user/user_model.dart';
import 'package:unifood/utils/image_uploader.dart' as image_uploader;
import 'package:unifood/utils/user_profile_image.dart';

class AuthProvider with ChangeNotifier {
  final UserRepository? userRepository;
  final Logger? log;
  final FirebaseAuth _auth;

  UserModel? _user;

  AuthProvider({this.userRepository, this.log})
      : _auth = FirebaseAuth.instance {
    if (userRepository != null) {
      userRepository!.getFirstUser().then((userModel) {
        _user = userModel;
        _saveNetworkImageToFile(_user?.imageUrl);
        notifyListeners();
        if (log != null) {
          log!.d("Usuario por constuctor $userModel");
        }
      });
    }
  }

  Stream<UserModel?>? get userStream {
    try {
      return _auth.authStateChanges().asyncMap<UserModel?>(
        (firebaseUser) async {
          if (firebaseUser == null) {
            _user = null;
            if (userRepository != null) {
              userRepository!.deleteAll();
            }
          } else if ((_user?.id != firebaseUser.uid || _user == null) &&
              userRepository != null) {
            try {
              _user = await userRepository!.getUserById(firebaseUser.uid);
              _saveNetworkImageToFile(_user?.imageUrl);
              notifyListeners();
              if (log != null) {
                log!.d("usuario actualizado $user");
              }
            } catch (e) {
              if (log != null) {
                log!.e("error al actualizar el usuario", error: e);
              }
            }
          }
          return _user;
        },
      );
    } catch (e) {
      if (_user != null) {
        return Stream.value(_user);
      }
    }
    return null;
  }

  UserModel? get user => _user;

  Future<void> signInWithEmailAndPassword({
    required String email,
    required String password,
  }) async {
    await _auth.signInWithEmailAndPassword(
      email: email,
      password: password,
    );
    notifyListeners();
  }

  Future<void> uploadImage(String imagePath) async {
    if (_user == null || userRepository == null) {
      throw Exception();
    }
    if (!userRepository!.connectivityProvider.isConnected) {
      throw NetworkException(statusCode: 404);
    }
    //OFFLINE
    try {
      await saveLocalImageToFile(imagePath);
      notifyListeners();
    } catch (e) {
      if (log != null) {
        log!.e("error al actualizar la imagen offline", error: e);
      }
      rethrow;
    }

    //ONLINE
    _uploadImage(imagePath);
  }

  Future<void> _uploadImage(String imagePath) async {
    try {
      final imageUrl =
          await image_uploader.uploadImage(imagePath, UserModel.storagePath);
      if (imageUrl != null) {
        _user = _user?.copyWith(imageUrl: imageUrl);
        await _saveNetworkImageToFile(imageUrl);
        await userRepository!.setUser(_user!);
      }
    } catch (e) {
      if (log != null) {
        log!.e("Error al subir la imagen onLine");
      }
      rethrow;
    }
  }

  Future<void> createUserWithEmailAndPassword({
    required String email,
    required String password,
    required String name,
    required String imagePath,
  }) async {
    String imageUrl = genericProfileImageUrl;
    final credential = await _auth.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );
    if (credential.user == null || userRepository == null) {
      return;
    }
    if (imagePath.isNotEmpty) {
      imageUrl = await image_uploader.uploadImage(
            imagePath,
            UserModel.storagePath,
          ) ??
          genericProfileImageUrl;
    }

    await userRepository!.setUser(
      UserModel(
        name: name,
        email: email,
        id: credential.user!.uid,
        imageUrl: imageUrl,
        createdAt: DateTime.now(),
        updatedAt: DateTime.now(),
      ),
    );
    await signInWithEmailAndPassword(email: email, password: password);
    notifyListeners();
  }

  Future<void> signOut() async {
    await _auth.signOut();
    if (userRepository == null) {
      return;
    }
    userRepository!.deleteAll();
  }

  Future<void> _saveNetworkImageToFile(String? imageUrl) async {
    if (imageUrl == null || imageUrl.isEmpty) {
      return;
    }
    final response = await http.get(Uri.parse(imageUrl));
    if (response.statusCode == 200) {
      final imagePath = await userProfileImagePath;
      final File file = File(imagePath);
      await file.writeAsBytes(response.bodyBytes);

      if (log != null) {
        log!.d('Image saved to: $imagePath');
      }
    } else {
      if (log != null) {
        log!.d('Failed to load image. Status code: ${response.statusCode}');
      }
    }
  }

  Future<void> saveLocalImageToFile(String sourceImagePath) async {
    final sourceFile = File(sourceImagePath);
    final imageContent = await sourceFile.readAsBytes();
    final imageDestinationPath = await userProfileImagePath;
    final File destinationImage = File(imageDestinationPath);
    await destinationImage.writeAsBytes(imageContent);
  }

  Future<File> get userProfileImageFile async {
    final imagePath = await userProfileImagePath;
    return File(imagePath);
  }
}
