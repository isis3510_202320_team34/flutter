import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:logger/logger.dart';
import 'package:unifood/data/respository/restaurant_repository.dart';
import 'package:unifood/domain/model/restaurant/restaurant_model.dart';
import 'package:unifood/utils/distance.dart';

import 'package:unifood/utils/location.dart';

class RestaurantProvider with ChangeNotifier {
  final RestaurantRepository? restaurantRepository;
  final Logger? log;
  List<RestaurantModel> _restaurants = [];
  Set<String> _categories = {};

  RestaurantProvider({this.log, this.restaurantRepository});

  List<RestaurantModel> get restaurants => UnmodifiableListView(_restaurants);

  Set<String> get categories => UnmodifiableSetView(_categories);

  Future<void> listRestaurants() async {
    if (restaurantRepository == null) {
      return;
    }
    _restaurants = await restaurantRepository!.listRestaurants();
    notifyListeners();
    compute<List<RestaurantModel>, Set<String>>(
      _computeCategories,
      _restaurants,
    ).then((categories) {
      _categories = categories;
    });
    compute<List<RestaurantModel>, List<RestaurantModel>>(
      _computeDistances,
      _restaurants,
    ).then((computedRestaurants) {
      _restaurants = computedRestaurants;
      notifyListeners();
    });
  }

  Future<void> _deletePersistedRestaurants() async {
    if (restaurantRepository == null) {
      return;
    }
    await restaurantRepository!.deleteAll();
    _restaurants = [];
    notifyListeners();
  }

  Future<void> updateRestaurants() async {
    await _deletePersistedRestaurants();
    await listRestaurants();
  }
}

List<RestaurantModel> _computeDistances(
  List<RestaurantModel> restaurants,
) {
  final List<RestaurantModel> computedRestaurants = [];
  Location location;
  double distance;
  RestaurantModel computedRestaurant;
  for (final restaurant in restaurants) {
    location = Location();
    distance = calculateDistanceFromCurrentLocation(
      location,
      restaurant.latitude,
      restaurant.longitude,
    );

    computedRestaurant = restaurant.copyWith(distance: distance);
    computedRestaurants.add(computedRestaurant);
  }
  return computedRestaurants;
}

Set<String> _computeCategories(List<RestaurantModel> restaurants) {
  return restaurants.map((restaurant) => restaurant.category).toSet();
}
