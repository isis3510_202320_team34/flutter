import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:unifood/data/network/entity/review/review_entity.dart';
import 'package:unifood/data/respository/review_repository.dart';
import 'package:unifood/domain/exception/network_exception.dart';
import 'package:unifood/domain/model/review/review_model.dart';
import 'package:unifood/domain/model/user/user_model.dart';
import 'package:unifood/presentation/providers/auth_provider.dart';
import 'package:unifood/utils/image_uploader.dart';
import 'package:unifood/utils/lru_Cache.dart';
import 'package:uuid/uuid.dart';

class ReviewProvider with ChangeNotifier {
  final AuthProvider? authProvider;
  final ReviewRepository? reviewRepository;
  final Logger? log;

  final LRUCache<String, List<ReviewModel>> _restaurantsReviews = LRUCache(3);
  List<ReviewModel> _userReviews = [];
  final LRUCache<String, bool> _canPostCache = LRUCache(10);

  ReviewProvider({this.log, this.reviewRepository, this.authProvider});
  List<ReviewModel>? restaurantReviews(String id) {
    return _restaurantsReviews.get(id) != null
        ? UnmodifiableListView(_restaurantsReviews.get(id)!)
        : null;
  }

  List<ReviewModel> get userReviews => UnmodifiableListView(_userReviews);

  void addReview({
    required String content,
    required double rate,
    required List<String> imagesPath,
    required String restaurantId,
  }) {
    if (authProvider?.user == null || reviewRepository == null) {
      if (log != null) {
        log!.e("Error no hay usuario o repositorio de reviews");
      }
      throw Exception();
    }
    if (!reviewRepository!.connectivityProvider.isConnected) {
      if (log != null) {
        log!.e("No hay internet para subir el review");
      }
      throw NetworkException(statusCode: 404);
    }
    final review = ReviewModel(
      content: content,
      id: const Uuid().v4(),
      rate: rate,
      restaurantId: restaurantId,
      userId: authProvider!.user!.id,
      createdAt: DateTime.now(),
      updatedAt: DateTime.now(),
    );

    if (_restaurantsReviews.get(restaurantId) == null) {
      _restaurantsReviews.put(restaurantId, [review]);
    } else {
      _restaurantsReviews.get(restaurantId)!.add(review);
    }
    notifyListeners();

    _addReview(
      content: content,
      rate: rate,
      imagesPath: imagesPath,
      restaurantId: restaurantId,
    );
  }

  Future<void> _addReview({
    required String content,
    required double rate,
    required List<String> imagesPath,
    required String restaurantId,
  }) async {
    if (authProvider?.user == null || reviewRepository == null) {
      if (log != null) {
        log!.e("Error no hay usuario o repositorio de reviews");
      }
      throw Exception();
    }
    if (!reviewRepository!.connectivityProvider.isConnected) {
      if (log != null) {
        log!.e("No hay internet para subir el review");
      }
      throw NetworkException(statusCode: 404);
    }
    final List<String> imageUrls = [];
    String? imageUrl;
    for (final imagePath in imagesPath) {
      imageUrl = await uploadImage(imagePath, ReviewEntity.collectionPath);
      if (imageUrl != null) {
        imageUrls.add(imageUrl);
      }
    }
    final review = ReviewModel(
      content: content,
      id: const Uuid().v4(),
      rate: rate,
      imagesUrl: imageUrls,
      restaurantId: restaurantId,
      userId: authProvider!.user!.id,
      createdAt: DateTime.now(),
      updatedAt: DateTime.now(),
    );
    reviewRepository!.setReview(review);

    notifyListeners();
  }

  Future<void> listRestaurantReviews(String restaurantId) async {
    if (reviewRepository == null) {
      return;
    }
    final reviews =
        await reviewRepository!.listReviewsFromRestaurant(restaurantId);
    _restaurantsReviews.put(restaurantId, reviews);
    notifyListeners();
  }

  Future<void> listUserReviews() async {
    final user = await authProvider?.userStream?.last;

    if (user == null || reviewRepository == null) {
      return;
    }

    _userReviews = await reviewRepository!.listReviewsFromUser(user.id);
    notifyListeners();
  }

  Future<bool> canPost(String restaurantId) async {
    final fromCache = _canPostCache.get(restaurantId);
    if (fromCache != null) {
      return fromCache;
    }
    bool canPost = true;
    if (authProvider?.user == null || reviewRepository == null) {
      canPost = false;
    } else {
      final reviews = await reviewRepository!.listReviewsFromRestaurant(
        restaurantId,
      );
      final index = reviews.indexWhere(
        (element) => element.userId == authProvider!.user!.id,
      );
      if (index != -1) {
        canPost = false;
      }
    }
    _canPostCache.put(restaurantId, canPost);
    return canPost;
  }

  Future<UserModel?> getUserByReview(ReviewModel review) async {
    if (reviewRepository == null) {
      return null;
    }
    return await reviewRepository!.getUserByReview(review);
  }
}
