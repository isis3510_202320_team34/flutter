import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:unifood/presentation/widgets/error_snackbar.dart';
import 'package:unifood/presentation/widgets/success_snackbar.dart';

class ConnectivityProvider {
  bool _isConnected = true;
  final Logger log;
  final GlobalKey<ScaffoldMessengerState> key;

  ConnectivityProvider({required this.log, required this.key}) {
    final futureResult = Connectivity().checkConnectivity();
    futureResult.then(
      (result) {
        _setInternetConnected(result);
      },
    );
    Connectivity().onConnectivityChanged.listen(_setInternetConnected);
  }

  bool get isConnected {
    log.d("_isConnected $_isConnected");
    return _isConnected;
  }

  void _setInternetConnected(ConnectivityResult? result) {
    if (result == ConnectivityResult.none) {
      log.d("Without Internet $result");
      _isConnected = false;
      if (key.currentState != null) {
        key.currentState!.showSnackBar(
          ErrorSnackbar(
            error: "Offline",
          ),
        );
      }
    } else if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      log.d("With Internet $result");
      if (key.currentState != null && _isConnected == false) {
        key.currentState!.showSnackBar(
          SuccessSnackbar(
            success: "Online",
          ),
        );
      }
      _isConnected = true;
    }
  }
}
