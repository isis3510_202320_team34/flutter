import 'package:logger/logger.dart';
import 'package:unifood/data/network/entity/restaurant/restaurant_entity.dart';
import 'package:unifood/data/network/entity/review/review_entity.dart';
import 'package:unifood/data/network/entity/user/user_entity.dart';
import 'package:unifood/domain/exception/mapper_exception.dart';
import 'package:unifood/domain/model/restaurant/restaurant_model.dart';
import 'package:unifood/domain/model/review/review_model.dart';

import 'package:unifood/domain/model/user/user_model.dart';

class NetworkMapper {
  final Logger log;

  NetworkMapper({required this.log});

  ReviewModel toReview(ReviewEntity entity) {
    try {
      return ReviewModel(
        id: entity.id,
        content: entity.content,
        userId: entity.userId,
        restaurantId: entity.restaurantId,
        rate: entity.rate,
        createdAt: entity.createdAt,
        updatedAt: entity.updatedAt,
      );
    } catch (e) {
      throw MapperException<ReviewEntity, ReviewModel>(e.toString());
    }
  }

  List<ReviewModel> toReviews(List<ReviewEntity> entities) {
    final List<ReviewModel> reviews = [];
    ReviewModel review;
    for (final entity in entities) {
      try {
        review = toReview(entity);
        reviews.add(review);
      } on MapperException catch (e) {
        log.e("Could not entity $entity", error: e);
      }
    }
    return reviews;
  }

  ReviewEntity toReviewEntity(ReviewModel review) {
    try {
      return ReviewEntity(
        id: review.id,
        content: review.content,
        userId: review.userId,
        restaurantId: review.restaurantId,
        rate: review.rate,
        createdAt: review.createdAt,
        updatedAt: review.updatedAt,
      );
    } catch (e) {
      throw MapperException<ReviewModel, ReviewEntity>(e.toString());
    }
  }

  UserModel toUser(UserEntity entity) {
    try {
      return UserModel(
        id: entity.id,
        createdAt: entity.createdAt,
        email: entity.email,
        imageUrl: entity.imageUrl,
        name: entity.name,
        updatedAt: entity.updatedAt,
      );
    } catch (e) {
      throw MapperException<UserEntity, UserModel>(e.toString());
    }
  }

  List<UserModel> toUsers(List<UserEntity> entities) {
    final List<UserModel> users = [];
    UserModel user;
    for (final entity in entities) {
      try {
        user = toUser(entity);
        users.add(user);
      } on MapperException catch (e) {
        log.w("Could not entity ${entity.id}", error: e);
      }
    }
    return users;
  }

  UserEntity toUserEntity(UserModel user) {
    try {
      return UserEntity(
        id: user.id,
        createdAt: user.createdAt,
        email: user.email,
        imageUrl: user.imageUrl,
        name: user.name,
        updatedAt: user.updatedAt,
      );
    } catch (e) {
      throw MapperException<ReviewModel, ReviewEntity>(e.toString());
    }
  }

  RestaurantModel toRestaurant(RestaurantEntity entity) {
    try {
      return RestaurantModel(
        id: entity.id,
        name: entity.name,
        address: entity.address,
        category: entity.category,
        description: entity.description,
        imageName: entity.imageName,
        isOpen: entity.isOpen,
        latitude: entity.latitude,
        longitude: entity.longitude,
        phone: entity.phone,
        rate: entity.rate,
        schedule: entity.schedule,
      );
    } catch (e) {
      throw MapperException<RestaurantEntity, RestaurantModel>(e.toString());
    }
  }

  List<RestaurantModel> toRestaurants(List<RestaurantEntity> entities) {
    final List<RestaurantModel> restaurants = [];
    RestaurantModel restaurant;
    for (final entity in entities) {
      try {
        restaurant = toRestaurant(entity);
        restaurants.add(restaurant);
      } on MapperException catch (e) {
        log.w("Could not entity ${entity.id}", error: e);
      }
    }
    return restaurants;
  }
}
