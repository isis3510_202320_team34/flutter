import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:unifood/data/network/entity/restaurant/restaurant_entity.dart';
import 'package:unifood/data/network/entity/review/review_entity.dart';
import 'package:unifood/data/network/entity/user/user_entity.dart';

class ApiClient {
  final _db = FirebaseFirestore.instance;
  final connectivity = Connectivity();

  Future<List<ReviewEntity>> listReviewsFromRestaurant(
    String restaurantId, {
    int limit = 20,
  }) async {
    try {
      final List<ReviewEntity> reviewEntities = [];
      final Query<Map<String, dynamic>> query = _db
          .collection(ReviewEntity.collectionPath)
          .where('restaurantId', isEqualTo: restaurantId)
          .orderBy("createdAt")
          .limit(limit);
      final snapshot = await query.get();
      Map<String, dynamic> data;
      for (final doc in snapshot.docs) {
        data = {
          ...doc.data(),
          "id": doc.id,
        };
        reviewEntities.add(ReviewEntity.fromJson(data));
      }
      return reviewEntities;
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<List<ReviewEntity>> listReviewsFromUser(
    String userId, {
    int limit = 20,
  }) async {
    try {
      final List<ReviewEntity> reviewEntities = [];
      final Query<Map<String, dynamic>> query = _db
          .collection(ReviewEntity.collectionPath)
          .where('userId', isEqualTo: userId)
          .orderBy("createdAt")
          .limit(limit);
      final snapshot = await query.get();
      Map<String, dynamic> data;
      for (final doc in snapshot.docs) {
        data = {
          ...doc.data(),
          "id": doc.id,
        };
        reviewEntities.add(ReviewEntity.fromJson(data));
      }
      return reviewEntities;
    } catch (e) {
      throw Exception('Unknown error');
    }
  }

  Future<void> setReview(ReviewEntity entity) async {
    await _db
        .collection(ReviewEntity.collectionPath)
        .doc(entity.id)
        .set(entity.toJson());
  }

  Future<UserEntity?> getUserById(String id) async {
    final snapshot =
        await _db.collection(UserEntity.collectionPath).doc(id).get();
    if (snapshot.data() == null) {
      return null;
    }
    final data = {...snapshot.data()!, "id": snapshot.id};
    return UserEntity.fromJson(data);
  }

  Future<void> setUser(UserEntity user) async {
    await _db.collection(UserEntity.collectionPath).doc(user.id).set(
          user.toJson(),
        );
  }

  Future<List<RestaurantEntity>> listRestaurants() async {
    final List<RestaurantEntity> restaurants = [];
    final snapshot = await _db
        .collection(RestaurantEntity.collectionPath)
        .orderBy("rate", descending: true)
        .get();
    Map<String, dynamic> data;
    for (final doc in snapshot.docs) {
      data = {
        ...doc.data(),
        "id": doc.id,
      };
      restaurants.add(
        RestaurantEntity.fromJson(
          data,
        ),
      );
    }
    return restaurants;
  }

  Future<RestaurantEntity?> getRestaurantById(String id) async {
    final snapshot =
        await _db.collection(RestaurantEntity.collectionPath).doc(id).get();
    if (snapshot.data() == null) {
      return null;
    }
    final data = {...snapshot.data()!, "id": snapshot.id};
    return RestaurantEntity.fromJson(data);
  }
}
