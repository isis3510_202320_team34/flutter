import 'package:freezed_annotation/freezed_annotation.dart';

part 'review_entity.g.dart';

@JsonSerializable()
class ReviewEntity {
  static const String collectionPath = "reviews-flutter";
  final String id;
  final String content;
  final double rate;
  final String userId;
  final String restaurantId;
  final DateTime createdAt;
  final DateTime updatedAt;
  final List<String> imagesUrl;

  ReviewEntity({
    required this.id,
    required this.content,
    required this.userId,
    required this.restaurantId,
    required this.rate,
    required this.createdAt,
    required this.updatedAt,
    this.imagesUrl = const [],
  });

  factory ReviewEntity.fromJson(Map<String, dynamic> json) =>
      _$ReviewEntityFromJson(json);

  Map<String, dynamic> toJson() => _$ReviewEntityToJson(this);
}
