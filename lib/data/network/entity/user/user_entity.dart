import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_entity.g.dart';

@JsonSerializable()
class UserEntity {
  static const String collectionPath = "users-flutter";
  final String id;
  final String name;
  final String email;
  final String imageUrl;
  final DateTime createdAt;
  final DateTime updatedAt;

  UserEntity({
    required this.id,
    required this.name,
    required this.email,
    required this.imageUrl,
    required this.createdAt,
    required this.updatedAt,
  });

  factory UserEntity.fromJson(Map<String, dynamic> json) =>
      _$UserEntityFromJson(json);

  Map<String, dynamic> toJson() => _$UserEntityToJson(this);
}
