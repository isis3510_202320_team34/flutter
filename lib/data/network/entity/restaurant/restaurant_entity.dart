import 'package:freezed_annotation/freezed_annotation.dart';

part 'restaurant_entity.g.dart';

@JsonSerializable()
class RestaurantEntity {
  static const String collectionPath = "restaurants";
  final String id;
  final String name;
  final String address;
  final String category;
  final String description;
  final String imageName;
  final bool isOpen;
  final double latitude;
  final double longitude;
  final List<MenuItemEntity> menu;
  final String phone;
  final double rate;
  final String schedule;

  RestaurantEntity({
    required this.id,
    required this.name,
    required this.address,
    required this.category,
    required this.description,
    required this.imageName,
    required this.isOpen,
    required this.latitude,
    required this.longitude,
    required this.menu,
    required this.phone,
    required this.rate,
    required this.schedule,
  });

  factory RestaurantEntity.fromJson(Map<String, dynamic> json) =>
      _$RestaurantEntityFromJson(json);
}

@JsonSerializable()
class MenuItemEntity {
  final String description;
  final String id;
  final String imageName;
  final String name;
  final int prepTime;
  final int price;

  MenuItemEntity({
    required this.description,
    required this.id,
    required this.imageName,
    required this.name,
    required this.prepTime,
    required this.price,
  });

  factory MenuItemEntity.fromJson(Map<String, dynamic> json) =>
      _$MenuItemEntityFromJson(json);
}
