// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'restaurant_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RestaurantEntity _$RestaurantEntityFromJson(Map<String, dynamic> json) =>
    RestaurantEntity(
      id: json['id'] as String,
      name: json['name'] as String,
      address: json['address'] as String,
      category: json['category'] as String,
      description: json['description'] as String,
      imageName: json['imageName'] as String,
      isOpen: json['isOpen'] as bool,
      latitude: (json['latitude'] as num).toDouble(),
      longitude: (json['longitude'] as num).toDouble(),
      menu: (json['menu'] as List<dynamic>)
          .map((e) => MenuItemEntity.fromJson(e as Map<String, dynamic>))
          .toList(),
      phone: json['phone'] as String,
      rate: (json['rate'] as num).toDouble(),
      schedule: json['schedule'] as String,
    );

Map<String, dynamic> _$RestaurantEntityToJson(RestaurantEntity instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'address': instance.address,
      'category': instance.category,
      'description': instance.description,
      'imageName': instance.imageName,
      'isOpen': instance.isOpen,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'menu': instance.menu,
      'phone': instance.phone,
      'rate': instance.rate,
      'schedule': instance.schedule,
    };

MenuItemEntity _$MenuItemEntityFromJson(Map<String, dynamic> json) =>
    MenuItemEntity(
      description: json['description'] as String,
      id: json['id'] as String,
      imageName: json['imageName'] as String,
      name: json['name'] as String,
      prepTime: json['prepTime'] as int,
      price: json['price'] as int,
    );

Map<String, dynamic> _$MenuItemEntityToJson(MenuItemEntity instance) =>
    <String, dynamic>{
      'description': instance.description,
      'id': instance.id,
      'imageName': instance.imageName,
      'name': instance.name,
      'prepTime': instance.prepTime,
      'price': instance.price,
    };
