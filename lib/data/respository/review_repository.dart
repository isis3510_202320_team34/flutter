import 'package:logger/logger.dart';
import 'package:unifood/data/database/dao/reviews_dao.dart';
import 'package:unifood/data/database/database_mapper.dart';
import 'package:unifood/data/network/client/api_client.dart';
import 'package:unifood/data/network/network_mapper.dart';
import 'package:unifood/domain/exception/network_exception.dart';
import 'package:unifood/domain/model/review/review_model.dart';
import 'package:unifood/domain/model/user/user_model.dart';
import 'package:unifood/presentation/providers/connectivity_provider.dart';

class ReviewRepository {
  final ApiClient apiClient;
  final NetworkMapper networkMapper;
  final ReviewsDao reviewsDao;
  final DatabaseMapper databaseMapper;
  final ConnectivityProvider connectivityProvider;
  final Logger log;

  ReviewRepository({
    required this.reviewsDao,
    required this.databaseMapper,
    required this.apiClient,
    required this.networkMapper,
    required this.connectivityProvider,
    required this.log,
  });

  Future<List<ReviewModel>> listReviewsFromRestaurant(
    String restaurantId, {
    int limit = 20,
  }) async {
    if (connectivityProvider.isConnected) {
      final entities =
          await apiClient.listReviewsFromRestaurant(restaurantId, limit: limit);
      final reviews = networkMapper.toReviews(entities);

      return reviews;
    } else {
      log.d("No Network");
      throw NetworkException(statusCode: 404);
    }
  }

  Future<List<ReviewModel>> listReviewsFromUser(
    String userId, {
    bool network = false,
    int limit = 20,
  }) async {
    if (connectivityProvider.isConnected) {
      final entities =
          await apiClient.listReviewsFromUser(userId, limit: limit);
      final reviews = networkMapper.toReviews(entities);
      return reviews;
    } else {
      throw NetworkException(statusCode: 404);
    }
  }

  Future<void> setReview(ReviewModel review) async {
    if (connectivityProvider.isConnected) {
      final entity = networkMapper.toReviewEntity(review);
      apiClient.setReview(entity);
    } else {
      log.d("No Network");
      throw NetworkException(statusCode: 404);
    }
  }

  Future<UserModel?> getUserByReview(
    ReviewModel review,
  ) async {
    if (connectivityProvider.isConnected) {
      final entity = await apiClient.getUserById(review.userId);
      if (entity == null) {
        return null;
      }
      final user = networkMapper.toUser(entity);
      return user;
    } else {
      log.d("No Network");
      throw NetworkException(statusCode: 404);
    }
  }
}
