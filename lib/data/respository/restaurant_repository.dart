import 'package:logger/logger.dart';
import 'package:unifood/data/database/dao/restaurant_dao.dart';
import 'package:unifood/data/database/database_mapper.dart';
import 'package:unifood/data/database/entity/restaurant/restaurant_db_entity.dart';
import 'package:unifood/data/network/client/api_client.dart';
import 'package:unifood/data/network/network_mapper.dart';
import 'package:unifood/domain/exception/network_exception.dart';
import 'package:unifood/domain/model/restaurant/restaurant_model.dart';
import 'package:unifood/presentation/providers/connectivity_provider.dart';

class RestaurantRepository {
  final ApiClient apiClient;
  final NetworkMapper networkMapper;
  final RestaurantsDao restaurantsDao;
  final DatabaseMapper databaseMapper;
  final ConnectivityProvider connectivityProvider;
  final Logger log;

  RestaurantRepository({
    required this.apiClient,
    required this.networkMapper,
    required this.restaurantsDao,
    required this.databaseMapper,
    required this.connectivityProvider,
    required this.log,
  });

  Future<List<RestaurantModel>> listRestaurants() async {
    final List<RestaurantDbEntity>? dbEntities =
        await restaurantsDao.selectAll();
    if (dbEntities != null && dbEntities.isNotEmpty) {
      log.d("Local");
      return databaseMapper.toRestaurants(dbEntities);
    }
    if (connectivityProvider.isConnected) {
      log.d("Network");
      final entities = await apiClient.listRestaurants();
      final restaurants = networkMapper.toRestaurants(entities);
      _setRestaurantsLocal(restaurants);
      return restaurants;
    } else {
      log.d("No Network");
      throw NetworkException(statusCode: 404);
    }
  }

  Future<void> deleteAll() async => await restaurantsDao.deleteAll();

  Future<void> _setRestaurantsLocal(
    List<RestaurantModel> restaurants,
  ) async {
    final dbEntities = databaseMapper.toRestaurantDbEntities(restaurants);
    await restaurantsDao.insertAll(dbEntities);
  }
}
