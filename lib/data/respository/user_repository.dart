import 'package:logger/logger.dart';
import 'package:unifood/data/database/dao/user_dao.dart';
import 'package:unifood/data/database/database_mapper.dart';
import 'package:unifood/data/network/client/api_client.dart';

import 'package:unifood/data/network/network_mapper.dart';
import 'package:unifood/domain/exception/network_exception.dart';
import 'package:unifood/domain/model/user/user_model.dart';
import 'package:unifood/presentation/providers/connectivity_provider.dart';

class UserRepository {
  final ApiClient apiClient;
  final NetworkMapper networkMapper;
  final UsersDao usersDao;
  final DatabaseMapper databaseMapper;
  final ConnectivityProvider connectivityProvider;
  final Logger log;

  UserRepository({
    required this.apiClient,
    required this.networkMapper,
    required this.usersDao,
    required this.databaseMapper,
    required this.connectivityProvider,
    required this.log,
  });

  Future<UserModel?> getUserById(String id) async {
    final dbEntity = await usersDao.select(id);
    if (dbEntity != null) {
      log.d("Local");
      return databaseMapper.toUser(dbEntity);
    } else if (connectivityProvider.isConnected) {
      log.d("Network");
      final entity = await apiClient.getUserById(id);
      if (entity == null) {
        log.d("No network entity");
        return null;
      }
      log.d("user");
      final user = networkMapper.toUser(entity);
      _setUserLocal(user);

      return user;
    } else {
      log.d("No Network");
      throw NetworkException(statusCode: 404);
    }
  }

  Future<UserModel?> getFirstUser() async {
    final entity = await usersDao.getFirst();
    if (entity == null) {
      return null;
    }
    final user = databaseMapper.toUser(entity);
    return user;
  }

  Future<void> setUser(UserModel user) async {
    if (connectivityProvider.isConnected) {
      final entity = networkMapper.toUserEntity(user);
      apiClient.setUser(entity);
      _setUserLocal(user);
    } else {
      log.d("No Network");
      throw NetworkException(statusCode: 404);
    }
  }

  Future<void> _setUserLocal(UserModel user) async {
    final userDbEntity = databaseMapper.toUserDbEntity(user);
    await usersDao.insert(userDbEntity);
  }

  Future<void> deleteAll() async => await usersDao.deleteAll();
}
