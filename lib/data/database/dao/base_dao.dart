import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:unifood/data/database/entity/restaurant/restaurant_db_entity.dart';
import 'package:unifood/data/database/entity/review/review_db_entity.dart';
import 'package:unifood/data/database/entity/user/user_db_entity.dart';

abstract class BaseDao {
  static const version = 1;

  static const _databaseName = 'com.my.app.db';

  static const reviewsTableName = "reviews";
  static const restaurantsTableName = "restaurants";
  static const usersTableName = "users";

  Database? _database;

  @protected
  Future<Database> getDb() async {
    _database ??= await _getDatabase();
    return _database!;
  }

  Future<Database> _getDatabase() async {
    return await openDatabase(
      join(
        await getDatabasesPath(),
        _databaseName,
      ),
      onCreate: (db, version) async {
        final batch = db.batch();
        _createReviewsTable(batch);
        _createRestaurantsTable(batch);
        _createUsersTable(batch);
        await batch.commit();
      },
      version: version,
    );
  }

  void _createReviewsTable(Batch batch) {
    batch.execute('''
   CREATE TABLE $reviewsTableName (
      ${ReviewDbEntity.fieldId} TEXT PRIMARY KEY,
      ${ReviewDbEntity.fieldContent} TEXT NOT NULL,
      ${ReviewDbEntity.fieldRate} REAL NOT NULL,
      ${ReviewDbEntity.fieldUserId} TEXT NOT NULL,
      ${ReviewDbEntity.fieldRestaurantId} TEXT NOT NULL,
      ${ReviewDbEntity.fieldCreatedAt} TEXT NOT NULL,
      ${ReviewDbEntity.fieldUpdatedAt} TEXT NOT NULL,
      FOREIGN KEY (${ReviewDbEntity.fieldUserId}) REFERENCES $usersTableName(${UserDbEntity.fieldId}),
      FOREIGN KEY (${ReviewDbEntity.fieldRestaurantId}) REFERENCES $restaurantsTableName(${RestaurantDbEntity.fieldId})
    );
    CREATE INDEX idx_${reviewsTableName}_reviewId ON $reviewsTableName (${ReviewDbEntity.fieldId});
    CREATE INDEX idx_${reviewsTableName}_userId ON $reviewsTableName (${ReviewDbEntity.fieldUserId});
    CREATE INDEX idx_${reviewsTableName}_restaurantId ON $reviewsTableName (${ReviewDbEntity.fieldRestaurantId});
''');
  }

  void _createUsersTable(Batch batch) {
    batch.execute('''
      CREATE TABLE $usersTableName (
        ${UserDbEntity.fieldId} TEXT PRIMARY KEY,
        ${UserDbEntity.fieldName} TEXT NOT NULL,
        ${UserDbEntity.fieldEmail} TEXT NOT NULL,
        ${UserDbEntity.fieldImageUrl} TEXT NOT NULL,
        ${UserDbEntity.fieldCreatedAt} TEXT NOT NULL,
        ${UserDbEntity.fieldUpdatedAt} TEXT NOT NULL
    );
    CREATE INDEX idx_${usersTableName}_userId ON $usersTableName (${UserDbEntity.fieldId});
''');
  }

  void _createRestaurantsTable(Batch batch) {
    batch.execute('''
     CREATE TABLE $restaurantsTableName (
      ${RestaurantDbEntity.fieldId} TEXT PRIMARY KEY,
      ${RestaurantDbEntity.fieldImageName} TEXT NOT NULL,
      ${RestaurantDbEntity.fieldName} TEXT NOT NULL,
      ${RestaurantDbEntity.fieldAddress} TEXT NOT NULL,
      ${RestaurantDbEntity.fieldDescription} TEXT NOT NULL,
      ${RestaurantDbEntity.fieldCategory} TEXT NOT NULL,
      ${RestaurantDbEntity.fieldRate} REAL NOT NULL,
      ${RestaurantDbEntity.fieldIsOpen} INTEGER NOT NULL,
      ${RestaurantDbEntity.fieldLatitude} REAL NOT NULL,
      ${RestaurantDbEntity.fieldLongitude} REAL NOT NULL,
      ${RestaurantDbEntity.fieldPhone} TEXT NOT NULL,
      ${RestaurantDbEntity.fieldSchedule} TEXT NOT NULL
    );
    CREATE INDEX idx_${restaurantsTableName}_restaurantId ON $restaurantsTableName (${RestaurantDbEntity.fieldId});
''');
  }
}
