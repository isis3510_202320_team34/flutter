import 'package:logger/logger.dart';
import 'package:sqflite/sqflite.dart';
import 'package:unifood/data/database/dao/base_dao.dart';
import 'package:unifood/data/database/entity/review/review_db_entity.dart';

class ReviewsDao extends BaseDao {
  Logger log;
  ReviewsDao(this.log);
  Future<List<ReviewDbEntity>?> selectAllFromUser(
    String userId, {
    int? limit,
    int? offset,
  }) async {
    try {
      final db = await getDb();
      final List<Map<String, dynamic>> maps = await db.query(
        BaseDao.reviewsTableName,
        limit: limit,
        offset: offset,
        orderBy: '${ReviewDbEntity.fieldCreatedAt} ASC',
        where: 'userId = ?',
        whereArgs: [userId],
      );

      return List.generate(
        maps.length,
        (index) {
          return ReviewDbEntity.fromJson(maps[index]);
        },
      );
    } catch (e) {
      log.e("Error al seleccionar los reviews por usuario $userId", error: e);
    }
    return null;
  }

  Future<List<ReviewDbEntity>?> selectAllFromRestaurant(
    String restaurantId, {
    int? limit,
    int? offset,
  }) async {
    try {
      final db = await getDb();
      final List<Map<String, dynamic>> maps = await db.query(
        BaseDao.reviewsTableName,
        limit: limit,
        offset: offset,
        orderBy: '${ReviewDbEntity.fieldCreatedAt} ASC',
        where: '${ReviewDbEntity.fieldRestaurantId} = ?',
        whereArgs: [restaurantId],
      );

      return List.generate(
        maps.length,
        (index) {
          return ReviewDbEntity.fromJson(maps[index]);
        },
      );
    } catch (e) {
      log.e(
        "error al listar los reviews por restaurante $restaurantId",
        error: e,
      );
    }
    return null;
  }

  Future<void> insertAll(List<ReviewDbEntity> entities) async {
    try {
      final db = await getDb();
      await db.transaction((txn) async {
        for (final entity in entities) {
          txn.insert(
            BaseDao.reviewsTableName,
            entity.toJson(),
            conflictAlgorithm: ConflictAlgorithm.replace,
          );
        }
      });
    } catch (e) {
      log.e("error al insertar los reviews", error: e);
    }
  }

  Future<void> insert(ReviewDbEntity entity) async {
    try {
      final db = await getDb();
      await db.transaction((txn) async {
        txn.insert(
          BaseDao.reviewsTableName,
          entity.toJson(),
          conflictAlgorithm: ConflictAlgorithm.replace,
        );
      });
    } catch (e) {
      log.e("Error al insertar el review", error: e);
    }
  }
}
