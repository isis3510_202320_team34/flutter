import 'package:logger/logger.dart';
import 'package:sqflite/sqflite.dart';
import 'package:unifood/data/database/dao/base_dao.dart';
import 'package:unifood/data/database/entity/user/user_db_entity.dart';

class UsersDao extends BaseDao {
  Logger log;
  UsersDao(this.log);
  Future<List<UserDbEntity>?> selectAll({
    int? limit,
    int? offset,
  }) async {
    try {
      final db = await getDb();
      final List<Map<String, dynamic>> maps = await db.query(
        BaseDao.usersTableName,
        limit: limit,
        offset: offset,
        orderBy: '${UserDbEntity.fieldCreatedAt} ASC',
      );

      return List.generate(
        maps.length,
        (index) {
          return UserDbEntity.fromJson(maps[index]);
        },
      );
    } catch (e) {
      log.e("Error al seleccionar los usuarios", error: e);
    }
    return null;
  }

  Future<UserDbEntity?> select(String userId) async {
    try {
      final db = await getDb();
      final List<Map<String, dynamic>> maps = await db.query(
        BaseDao.usersTableName,
        where: '${UserDbEntity.fieldId} = ?',
        whereArgs: [userId],
        limit: 1,
      );
      if (maps.isEmpty) {
        return null;
      }
      return UserDbEntity.fromJson(maps.first);
    } catch (e) {
      log.e("Error al seleccionar el usuario $userId", error: e);
    }
    return null;
  }

  Future<UserDbEntity?> getFirst() async {
    try {
      final db = await getDb();
      final List<Map<String, dynamic>> maps = await db.query(
        BaseDao.usersTableName,
        where: '${UserDbEntity.fieldId} = ?',
        limit: 1,
      );
      if (maps.isEmpty) {
        return null;
      }
      return UserDbEntity.fromJson(maps.first);
    } catch (e) {
      log.e("Error al sleccionar el primer usuario", error: e);
    }
    return null;
  }

  Future<void> insertAll(List<UserDbEntity> entities) async {
    try {
      final db = await getDb();
      await db.transaction((txn) async {
        for (final entity in entities) {
          txn.insert(
            BaseDao.usersTableName,
            entity.toJson(),
            conflictAlgorithm: ConflictAlgorithm.replace,
          );
        }
      });
    } catch (e) {
      log.e("Error al insertar los usuarios", error: e);
    }
  }

  Future<void> insert(UserDbEntity entity) async {
    try {
      final db = await getDb();
      await db.transaction(
        (txn) async {
          txn.insert(
            BaseDao.usersTableName,
            entity.toJson(),
            conflictAlgorithm: ConflictAlgorithm.replace,
          );
        },
      );
    } catch (e) {
      log.e("Error al insertar el usuario $entity", error: e);
    }
  }

  Future<void> deleteAll() async {
    try {
      final db = await getDb();
      await db.delete(
        BaseDao.usersTableName,
      );
    } catch (e) {
      log.e("Error al eliminar los usuarios", error: e);
    }
  }
}
