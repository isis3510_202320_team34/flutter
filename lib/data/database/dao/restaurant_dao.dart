import 'package:logger/logger.dart';
import 'package:sqflite/sqflite.dart';
import 'package:unifood/data/database/dao/base_dao.dart';
import 'package:unifood/data/database/entity/restaurant/restaurant_db_entity.dart';

class RestaurantsDao extends BaseDao {
  Logger log;
  RestaurantsDao(this.log);
  Future<List<RestaurantDbEntity>?> selectAll({
    int? limit,
    int? offset,
  }) async {
    try {
      final db = await getDb();
      final List<Map<String, dynamic>> maps = await db.query(
        BaseDao.restaurantsTableName,
        limit: limit,
        offset: offset,
        orderBy: '${RestaurantDbEntity.fieldRate} DESC',
      );
      return List.generate(
        maps.length,
        (index) {
          return RestaurantDbEntity.fromJson(maps[index]);
        },
      );
    } catch (e) {
      log.e("Error al seleccionar los restaurantes", error: e);
    }
    return null;
  }

  Future<void> insertAll(List<RestaurantDbEntity> entities) async {
    try {
      final db = await getDb();
      await db.transaction((txn) async {
        for (final entity in entities) {
          txn.insert(
            BaseDao.restaurantsTableName,
            entity.toJson(),
            conflictAlgorithm: ConflictAlgorithm.replace,
          );
        }
      });
    } catch (e) {
      log.e("Error al insertar los restaurantes", error: e);
    }
  }

  Future<void> deleteAll() async {
    try {
      final db = await getDb();
      await db.delete(
        BaseDao.restaurantsTableName,
      );
    } catch (e) {
      log.e("Error al eliminar los restaurantes", error: e);
    }
  }
}
