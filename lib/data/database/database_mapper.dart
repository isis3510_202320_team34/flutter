import 'package:logger/logger.dart';
import 'package:unifood/data/database/entity/restaurant/restaurant_db_entity.dart';
import 'package:unifood/data/database/entity/review/review_db_entity.dart';
import 'package:unifood/data/database/entity/user/user_db_entity.dart';
import 'package:unifood/domain/exception/mapper_exception.dart';
import 'package:unifood/domain/model/restaurant/restaurant_model.dart';
import 'package:unifood/domain/model/review/review_model.dart';
import 'package:unifood/domain/model/user/user_model.dart';

class DatabaseMapper {
  final Logger log;

  DatabaseMapper({required this.log});

  ReviewModel toReview(ReviewDbEntity entity) {
    try {
      return ReviewModel(
        id: entity.id,
        content: entity.content,
        userId: entity.userId,
        restaurantId: entity.restaurantId,
        rate: entity.rate,
        createdAt: entity.createdAt,
        updatedAt: entity.updatedAt,
      );
    } catch (e) {
      throw MapperException<ReviewDbEntity, ReviewModel>(e.toString());
    }
  }

  List<ReviewModel> toReviews(List<ReviewDbEntity> entities) {
    final List<ReviewModel> reviews = [];
    for (final entity in entities) {
      try {
        final review = toReview(entity);
        reviews.add(review);
      } catch (e) {
        log.w('Could not map enity ${entity.id}', error: e);
      }
    }

    return reviews;
  }

  ReviewDbEntity toReviewDbEntity(ReviewModel review) {
    try {
      return ReviewDbEntity(
        id: review.id,
        content: review.content,
        rate: review.rate,
        userId: review.userId,
        restaurantId: review.restaurantId,
        createdAt: review.createdAt,
        updatedAt: review.updatedAt,
      );
    } catch (e) {
      throw MapperException<ReviewDbEntity, ReviewModel>(e.toString());
    }
  }

  List<ReviewDbEntity> toReviewDbEntities(List<ReviewModel> reviews) {
    final List<ReviewDbEntity> entities = [];
    for (final review in reviews) {
      final entity = toReviewDbEntity(review);
      entities.add(entity);
      try {} catch (e) {
        log.e("Ocurrio un error con el modelo ${review.id}", error: e);
      }
    }
    return entities;
  }

  UserModel toUser(UserDbEntity entity) {
    try {
      return UserModel(
        id: entity.id,
        email: entity.email,
        name: entity.name,
        imageUrl: entity.imageUrl,
        createdAt: entity.createdAt,
        updatedAt: entity.updatedAt,
      );
    } catch (e) {
      throw MapperException<UserDbEntity, UserModel>(e.toString());
    }
  }

  List<UserModel> toUsers(List<UserDbEntity> entities) {
    final List<UserModel> users = [];
    for (final entity in entities) {
      try {
        final user = toUser(entity);
        users.add(user);
      } catch (e) {
        log.w('Could not map enity ${entity.id}', error: e);
      }
    }

    return users;
  }

  UserDbEntity toUserDbEntity(UserModel user) {
    try {
      return UserDbEntity(
        id: user.id,
        email: user.email,
        imageUrl: user.imageUrl,
        name: user.name,
        createdAt: user.createdAt,
        updatedAt: user.updatedAt,
      );
    } catch (e) {
      throw MapperException<UserDbEntity, UserModel>(e.toString());
    }
  }

  List<UserDbEntity> toUserDbEntities(List<UserModel> users) {
    final List<UserDbEntity> entities = [];
    for (final user in users) {
      final entity = toUserDbEntity(user);
      entities.add(entity);
      try {} catch (e) {
        log.e("Ocurrio un error con el modelo ${user.id}", error: e);
      }
    }
    return entities;
  }

  RestaurantModel toRestaurant(RestaurantDbEntity entity) {
    try {
      return RestaurantModel(
        id: entity.id,
        address: entity.address,
        category: entity.category,
        description: entity.description,
        imageName: entity.imageName,
        isOpen: entity.isOpen == 1,
        latitude: entity.latitude,
        longitude: entity.longitude,
        name: entity.name,
        phone: entity.phone,
        rate: entity.rate,
        schedule: entity.schedule,
      );
    } catch (e) {
      throw MapperException<RestaurantDbEntity, RestaurantModel>(e.toString());
    }
  }

  List<RestaurantModel> toRestaurants(List<RestaurantDbEntity> entities) {
    final List<RestaurantModel> restaurants = [];
    for (final entity in entities) {
      try {
        final restaurant = toRestaurant(entity);
        restaurants.add(restaurant);
      } catch (e) {
        log.w('Could not map enity ${entity.id}', error: e);
      }
    }

    return restaurants;
  }

  RestaurantDbEntity toRestaurantDbEntity(RestaurantModel restaurant) {
    try {
      return RestaurantDbEntity(
        id: restaurant.id,
        name: restaurant.name,
        description: restaurant.description,
        address: restaurant.address,
        category: restaurant.category,
        imageName: restaurant.imageName,
        isOpen: restaurant.isOpen ? 1 : 0,
        latitude: restaurant.latitude,
        longitude: restaurant.longitude,
        phone: restaurant.phone,
        rate: restaurant.rate,
        schedule: restaurant.schedule,
      );
    } catch (e) {
      throw MapperException<RestaurantDbEntity, RestaurantModel>(e.toString());
    }
  }

  List<RestaurantDbEntity> toRestaurantDbEntities(
    List<RestaurantModel> restaurants,
  ) {
    final List<RestaurantDbEntity> entities = [];
    for (final rerestaurant in restaurants) {
      final entity = toRestaurantDbEntity(rerestaurant);
      entities.add(entity);
      try {} catch (e) {
        log.e("Ocurrio un error con el modelo ${rerestaurant.id}", error: e);
      }
    }
    return entities;
  }
}
