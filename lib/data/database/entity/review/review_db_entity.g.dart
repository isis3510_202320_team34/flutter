// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'review_db_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReviewDbEntity _$ReviewDbEntityFromJson(Map<String, dynamic> json) =>
    ReviewDbEntity(
      id: json['reviewId'] as String,
      content: json['reviewContent'] as String,
      rate: (json['reviewRate'] as num).toDouble(),
      userId: json['reviewUserId'] as String,
      restaurantId: json['reviewRestaurantId'] as String,
      createdAt: DateTime.parse(json['reviewCreatedAt'] as String),
      updatedAt: DateTime.parse(json['reviewUpdatedAt'] as String),
      imagesUrl: (json['reviewImagesUrl'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
    );

Map<String, dynamic> _$ReviewDbEntityToJson(ReviewDbEntity instance) =>
    <String, dynamic>{
      'reviewId': instance.id,
      'reviewContent': instance.content,
      'reviewRate': instance.rate,
      'reviewUserId': instance.userId,
      'reviewRestaurantId': instance.restaurantId,
      'reviewCreatedAt': instance.createdAt.toIso8601String(),
      'reviewUpdatedAt': instance.updatedAt.toIso8601String(),
      'reviewImagesUrl': instance.imagesUrl,
    };
