import 'package:freezed_annotation/freezed_annotation.dart';

part 'review_db_entity.g.dart';

@JsonSerializable()
class ReviewDbEntity {
  static const fieldId = 'reviewId';
  static const fieldContent = 'reviewContent';
  static const fieldRate = 'reviewRate';
  static const fieldUserId = 'reviewUserId';
  static const fieldRestaurantId = 'reviewRestaurantId';
  static const fieldCreatedAt = 'reviewCreatedAt';
  static const fieldUpdatedAt = 'reviewUpdatedAt';
  static const fieldImagesUrl = 'reviewImagesUrl';

  @JsonKey(name: fieldId)
  final String id;
  @JsonKey(name: fieldContent)
  final String content;
  @JsonKey(name: fieldRate)
  final double rate;
  @JsonKey(name: fieldUserId)
  final String userId;
  @JsonKey(name: fieldRestaurantId)
  final String restaurantId;
  @JsonKey(name: fieldCreatedAt)
  final DateTime createdAt;
  @JsonKey(name: fieldUpdatedAt)
  final DateTime updatedAt;
  @JsonKey(name: fieldImagesUrl)
  final List<String> imagesUrl;

  ReviewDbEntity({
    required this.id,
    required this.content,
    required this.rate,
    required this.userId,
    required this.restaurantId,
    required this.createdAt,
    required this.updatedAt,
    this.imagesUrl = const [],
  });

  factory ReviewDbEntity.fromJson(Map<String, dynamic> json) =>
      _$ReviewDbEntityFromJson(json);

  Map<String, dynamic> toJson() => _$ReviewDbEntityToJson(this);
}
