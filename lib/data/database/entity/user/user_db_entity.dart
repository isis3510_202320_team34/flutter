import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_db_entity.g.dart';

@JsonSerializable()
class UserDbEntity {
  static const fieldId = 'userId';
  static const fieldName = 'userName';
  static const fieldEmail = 'userEmail';
  static const fieldImageUrl = 'userImageUrl';
  static const fieldCreatedAt = 'userCreatedAt';
  static const fieldUpdatedAt = 'userUpdatedAt';

  @JsonKey(name: fieldId)
  final String id;
  @JsonKey(name: fieldName)
  final String name;
  @JsonKey(name: fieldEmail)
  final String email;
  @JsonKey(name: fieldImageUrl)
  final String imageUrl;
  @JsonKey(name: fieldCreatedAt)
  final DateTime createdAt;
  @JsonKey(name: fieldUpdatedAt)
  final DateTime updatedAt;

  UserDbEntity({
    required this.id,
    required this.name,
    required this.email,
    required this.imageUrl,
    required this.createdAt,
    required this.updatedAt,
  });

  factory UserDbEntity.fromJson(Map<String, dynamic> json) =>
      _$UserDbEntityFromJson(json);

  Map<String, dynamic> toJson() => _$UserDbEntityToJson(this);
}
