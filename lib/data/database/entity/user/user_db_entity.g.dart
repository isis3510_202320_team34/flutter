// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_db_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserDbEntity _$UserDbEntityFromJson(Map<String, dynamic> json) => UserDbEntity(
      id: json['userId'] as String,
      name: json['userName'] as String,
      email: json['userEmail'] as String,
      imageUrl: json['userImageUrl'] as String,
      createdAt: DateTime.parse(json['userCreatedAt'] as String),
      updatedAt: DateTime.parse(json['userUpdatedAt'] as String),
    );

Map<String, dynamic> _$UserDbEntityToJson(UserDbEntity instance) =>
    <String, dynamic>{
      'userId': instance.id,
      'userName': instance.name,
      'userEmail': instance.email,
      'userImageUrl': instance.imageUrl,
      'userCreatedAt': instance.createdAt.toIso8601String(),
      'userUpdatedAt': instance.updatedAt.toIso8601String(),
    };
