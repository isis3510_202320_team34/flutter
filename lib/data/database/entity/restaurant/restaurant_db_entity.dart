import 'package:freezed_annotation/freezed_annotation.dart';

part 'restaurant_db_entity.g.dart';

@JsonSerializable()
class RestaurantDbEntity {
  static const fieldId = 'restaurantId';
  static const fieldName = 'restaurantName';
  static const fieldAddress = 'restaurantAddress';
  static const fieldCategory = 'restaurantCategory';
  static const fieldDescription = 'restaurantDescription';
  static const fieldImageName = 'restaurantImageName';
  static const fieldIsOpen = 'restaurantIsOpen';
  static const fieldLatitude = 'restaurantLatitude';
  static const fieldLongitude = 'restaurantLongitude';
  static const fieldPhone = 'restaurantPhone';
  static const fieldRate = 'restaurantRate';
  static const fieldSchedule = 'restaurantSchedule';

  @JsonKey(name: fieldId)
  final String id;
  @JsonKey(name: fieldName)
  final String name;
  @JsonKey(name: fieldAddress)
  final String address;
  @JsonKey(name: fieldCategory)
  final String category;
  @JsonKey(name: fieldDescription)
  final String description;
  @JsonKey(name: fieldImageName)
  final String imageName;
  @JsonKey(name: fieldIsOpen)
  final int isOpen;
  @JsonKey(name: fieldLatitude)
  final double latitude;
  @JsonKey(name: fieldLongitude)
  final double longitude;
  @JsonKey(name: fieldPhone)
  final String phone;
  @JsonKey(name: fieldRate)
  final double rate;
  @JsonKey(name: fieldSchedule)
  final String schedule;

  RestaurantDbEntity({
    required this.id,
    required this.name,
    required this.address,
    required this.category,
    required this.description,
    required this.imageName,
    required this.isOpen,
    required this.latitude,
    required this.longitude,
    required this.phone,
    required this.rate,
    required this.schedule,
  });

  factory RestaurantDbEntity.fromJson(Map<String, dynamic> json) =>
      _$RestaurantDbEntityFromJson(json);

  Map<String, dynamic> toJson() => _$RestaurantDbEntityToJson(this);
}
