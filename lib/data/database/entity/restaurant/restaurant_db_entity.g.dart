// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'restaurant_db_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RestaurantDbEntity _$RestaurantDbEntityFromJson(Map<String, dynamic> json) =>
    RestaurantDbEntity(
      id: json['restaurantId'] as String,
      name: json['restaurantName'] as String,
      address: json['restaurantAddress'] as String,
      category: json['restaurantCategory'] as String,
      description: json['restaurantDescription'] as String,
      imageName: json['restaurantImageName'] as String,
      isOpen: json['restaurantIsOpen'] as int,
      latitude: (json['restaurantLatitude'] as num).toDouble(),
      longitude: (json['restaurantLongitude'] as num).toDouble(),
      phone: json['restaurantPhone'] as String,
      rate: (json['restaurantRate'] as num).toDouble(),
      schedule: json['restaurantSchedule'] as String,
    );

Map<String, dynamic> _$RestaurantDbEntityToJson(RestaurantDbEntity instance) =>
    <String, dynamic>{
      'restaurantId': instance.id,
      'restaurantName': instance.name,
      'restaurantAddress': instance.address,
      'restaurantCategory': instance.category,
      'restaurantDescription': instance.description,
      'restaurantImageName': instance.imageName,
      'restaurantIsOpen': instance.isOpen,
      'restaurantLatitude': instance.latitude,
      'restaurantLongitude': instance.longitude,
      'restaurantPhone': instance.phone,
      'restaurantRate': instance.rate,
      'restaurantSchedule': instance.schedule,
    };
