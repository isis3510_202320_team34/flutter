import 'package:flutter/material.dart';

class ShowError {
  BuildContext ctx;
  ShowError(this.ctx);

  void emit(String? error) {
    ScaffoldMessenger.of(ctx).showSnackBar(
      SnackBar(
        content: Column(
          children: [
            const Text('An error occurred!'),
            if (error != null) Text(error),
          ],
        ),
        backgroundColor: Colors.red,
      ),
    );
  }
}
