// ignore: file_names
import 'dart:collection' show LinkedHashMap;

class LRUCache<K, V> {
  final int capacity;
  final LinkedHashMap<K, V> _cache = LinkedHashMap<K, V>();

  LRUCache(this.capacity);

  V? get(K key) {
    if (_cache.containsKey(key)) {
      // Mover el elemento al frente para indicar que ha sido el más recientemente usado
      final value = _cache.remove(key);
      _cache[key] = value as V;
      return value;
    }
    return null;
  }

  void put(K key, V value) {
    if (_cache.length >= capacity) {
      // Eliminar el elemento menos recientemente usado (el primero en el orden de inserción)
      final keyToRemove = _cache.keys.first;
      _cache.remove(keyToRemove);
    }
    // Agregar el nuevo elemento al frente
    _cache[key] = value;
  }
}
