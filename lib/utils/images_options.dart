import 'package:flutter/material.dart';

Future<bool?> showImageOptionsModal(BuildContext ctx) async {
  return await showModalBottomSheet<bool>(
    context: ctx,
    builder: (BuildContext ctx) {
      return Wrap(
        children: <Widget>[
          ListTile(
            leading: const Icon(Icons.camera),
            title: const Text('Take a Photo'),
            onTap: () {
              Navigator.pop(ctx, false); // Close the bottom sheet
            },
          ),
          ListTile(
            leading: const Icon(Icons.photo),
            title: const Text('Choose from Gallery'),
            onTap: () {
              Navigator.pop(ctx, true); // Close the bottom sheet
            },
          ),
        ],
      );
    },
  );
}
