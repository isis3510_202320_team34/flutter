import 'dart:math';
import 'package:unifood/utils/location.dart';

double degreesToRadians(double degrees) {
  return degrees * pi / 180.0;
}

double calculateDistance(double lat1, double lon1, double lat2, double lon2) {
  const double earthRadius = 6371.0; // Radio de la Tierra en kilómetros

  // Convierte las latitudes y longitudes de grados a radianes
  final double lat1Rad = degreesToRadians(lat1);
  final double lon1Rad = degreesToRadians(lon1);
  final double lat2Rad = degreesToRadians(lat2);
  final double lon2Rad = degreesToRadians(lon2);

  // Diferencias de latitud y longitud
  final double latDiff = lat2Rad - lat1Rad;
  final double lonDiff = lon2Rad - lon1Rad;

  // Fórmula de Haversine
  final double a = sin(latDiff / 2) * sin(latDiff / 2) +
      cos(lat1Rad) * cos(lat2Rad) * sin(lonDiff / 2) * sin(lonDiff / 2);
  final double c = 2 * atan2(sqrt(a), sqrt(1 - a));
  final double distance = earthRadius * c;

  return distance;
}

double calculateDistanceFromCurrentLocation(
  Location location,
  double lat,
  double lng,
) {
  return calculateDistance(location.lat, location.lng, lat, lng);
}
