import 'package:geolocator/geolocator.dart';
import 'package:unifood/domain/exception/permission_exception.dart';

class Location {
  double _lat = 0;
  double _lng = 0;

  static bool initialized = false;
  static final Location _instance = Location._internal();

  static Future<void> init() async {
    if (!initialized) {
      await _handleLocationPermission();
      final position = await Geolocator.getCurrentPosition();
      _instance
        .._lat = position.latitude
        .._lng = position.longitude;
      initialized = true;
    }
  }

  factory Location() {
    return _instance;
  }

  Location._internal();

  static Future<void> _handleLocationPermission() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      throw PermissionException(
        'Location services are disabled. Please enable the services',
      );
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        throw PermissionException('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      throw PermissionException(
        'Location permissions are permanently denied, we cannot request permissions.',
      );
    }
  }

  double get lat => _lat;
  double get lng => _lng;
}
