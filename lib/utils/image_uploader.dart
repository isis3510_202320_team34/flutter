import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:firebase_storage/firebase_storage.dart';

import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:image/image.dart' as img;
import 'package:mime/mime.dart';
import 'package:unifood/domain/exception/prohibit_content_exception.dart';
import 'package:uuid/uuid.dart';

Uint8List loadImage(http.Response response) {
  // URL de la imagen que deseas cargar

  // Realiza la petición HTTP para obtener la imagen

  // Verifica si la petición fue exitosa (código 200)
  if (response.statusCode == 200) {
    // Convierte los bytes de la respuesta a un objeto Image
    final img.Image imagenOriginal = img.decodeImage(response.bodyBytes)!;

    // Redimensiona la imagen a 256x256
    final img.Image imagenRedimensionada =
        img.copyResize(imagenOriginal, height: 256);

    // Convierte la imagen redimensionada a bytes
    final Uint8List bytesRedimensionados =
        Uint8List.fromList(img.encodePng(imagenRedimensionada));

    return bytesRedimensionados;
  } else {
    // Si la petición no fue exitosa, devuelve null
    return Uint8List(0);
  }
}

Future<String?> uploadImage(
  String imagePath,
  String firebaseStoragePath,
) async {
  final imageFile = File(imagePath);
  await analizeImage(imagePath);
  final reference = FirebaseStorage.instance.ref(firebaseStoragePath);
  final uuid = const Uuid().v4();
  final imageToUpload = reference.child("$uuid.jpg");
  final snapshot = await imageToUpload.putFile(imageFile);
  return await snapshot.ref.getDownloadURL();
}

Future<void> analizeImage(String imagePath) async {
  const picpurifyUrl = 'https://www.picpurify.com/analyse/1.1';
  const apiKey = "AlcTjI8VEh6rn48B9Bs2CobZnRL0qFr7";
  const task = 'porn_moderation,drug_moderation,gore_moderation';

  final request = http.MultipartRequest('POST', Uri.parse(picpurifyUrl))
    ..fields['API_KEY'] = apiKey
    ..fields['task'] = task;

  // Determine the file's mime type
  final file = File(imagePath);
  final fileType = lookupMimeType(file.path);

  // Add the file to the request
  request.files.add(
    await http.MultipartFile.fromPath(
      'file_image',
      file.path,
      contentType: MediaType.parse(fileType!),
    ),
  );

  final response = await request.send();
  final responseJson = json.decode(await response.stream.bytesToString())
      as Map<String, dynamic>;
  final finalDecision = responseJson["final_decision"] as String;
  if (finalDecision != "OK") {
    throw ProhibitContentException();
  }
}
