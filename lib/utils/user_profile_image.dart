import 'package:path_provider/path_provider.dart';

Future<String> get userProfileImagePath async {
  final tempDir = await getTemporaryDirectory();
  return "${tempDir.path}/user_profile_image.jpg";
}
