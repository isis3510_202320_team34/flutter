import 'package:flutter/material.dart';
import 'package:unifood/domain/model/restaurant/restaurant_model.dart';
import 'package:unifood/presentation/screens/forgot_password_screen.dart';
import 'package:unifood/presentation/screens/home.dart';
import 'package:unifood/presentation/screens/login_screen.dart';
import 'package:unifood/presentation/screens/register_screen.dart';
import 'package:unifood/presentation/screens/restaurant_details_screen.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  final args = settings.arguments;
  switch (settings.name) {
    case HomePage.path:
      return MaterialPageRoute(
        builder: (context) => const HomePage(),
      );
    case Login.path:
      return MaterialPageRoute(
        builder: (context) => const Login(),
      );
    case Register.path:
      return MaterialPageRoute(builder: (context) => const Register());
    case RestaurantDetailsScreen.path:
      return MaterialPageRoute(
        builder: (context) => RestaurantDetailsScreen(
          restaurant: args! as RestaurantModel,
        ),
      );
    case ForgotPassword.path:
      return MaterialPageRoute(
        builder: (context) => const ForgotPassword(),
      );
    default:
      return MaterialPageRoute(
        builder: (context) => const HomePage(),
      );
  }
}
