import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

TextStyle navBarTextStyle(Set<MaterialState> states) {
  if (states.contains(MaterialState.selected)) {
    return const TextStyle(color: Colors.black, fontWeight: FontWeight.w500);
  } else {
    return TextStyle(color: Colors.grey[400]);
  }
}

IconThemeData navBarIconThemeData(Set<MaterialState> states) {
  if (states.contains(MaterialState.selected)) {
    return const IconThemeData(color: Colors.black);
  } else {
    return IconThemeData(color: Colors.grey[400]);
  }
}

final theme = ThemeData(
  colorScheme: ColorScheme.fromSeed(
    seedColor: const Color.fromARGB(255, 224, 73, 26),
  ),
  fontFamily: "Nunito",
  textTheme: GoogleFonts.nunitoTextTheme(),
  textSelectionTheme: const TextSelectionThemeData(cursorColor: Colors.black),
  navigationBarTheme: NavigationBarThemeData(
    elevation: 0,
    labelTextStyle: MaterialStateProperty.resolveWith(navBarTextStyle),
    iconTheme: MaterialStateProperty.resolveWith(navBarIconThemeData),
    labelBehavior: NavigationDestinationLabelBehavior.alwaysShow,
  ),
  tabBarTheme: const TabBarTheme(
    labelStyle: TextStyle(fontWeight: FontWeight.bold),
  ),
  cardTheme: const CardTheme(
    margin: EdgeInsets.symmetric(horizontal: 10),
    elevation: 5,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(10),
      ),
    ),
    surfaceTintColor: Colors.white,
  ),
  textButtonTheme: TextButtonThemeData(
    style: TextButton.styleFrom(
      disabledBackgroundColor: Colors.grey[800],
      shape: const StadiumBorder(),
      textStyle: const TextStyle(
        fontWeight: FontWeight.w600,
      ),
    ),
  ),
  outlinedButtonTheme: OutlinedButtonThemeData(
    style: OutlinedButton.styleFrom(
      shape: const StadiumBorder(),
      side: const BorderSide(color: Colors.deepPurple),
      textStyle: const TextStyle(
        fontWeight: FontWeight.w600,
      ),
    ),
  ),
  appBarTheme: const AppBarTheme(
    elevation: 0,
    shadowColor: Colors.transparent,
    surfaceTintColor: Colors.white,
    titleTextStyle: TextStyle(
      fontFamily: "Roboto",
      fontSize: 20,
      fontWeight: FontWeight.bold,
      color: Colors.black,
    ),
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
      shape: const StadiumBorder(),
      textStyle: const TextStyle(fontWeight: FontWeight.w600),
    ),
  ),
  inputDecorationTheme: const InputDecorationTheme(
    helperStyle: TextStyle(fontWeight: FontWeight.w700),
    labelStyle: TextStyle(
      fontSize: 24,
      fontWeight: FontWeight.bold,
    ),
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Colors.grey),
      borderRadius: BorderRadius.all(
        Radius.circular(10),
      ),
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(10),
      ),
    ),
    errorBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Colors.red),
    ),
    contentPadding: EdgeInsets.symmetric(vertical: 18, horizontal: 16),
    floatingLabelBehavior: FloatingLabelBehavior.always,
    hintStyle: TextStyle(color: Colors.black54),
  ),
  bottomSheetTheme: const BottomSheetThemeData(
    backgroundColor: Colors.white,
    modalBackgroundColor: Colors.white,
    surfaceTintColor: Colors.white,
  ),
  useMaterial3: true,
);
