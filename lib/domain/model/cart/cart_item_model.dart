import 'package:freezed_annotation/freezed_annotation.dart';
part 'cart_item_model.freezed.dart';

@freezed
class CartItemModel with _$CartItemModel {
  factory CartItemModel({
    required String id,
    required String name,
    required String quantity,
    required String price,
  }) = _CartItemModel;
}
