import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:unifood/domain/model/cart/cart_item_model.dart';
part 'cart_model.freezed.dart';

@freezed
class CartModel with _$CartModel {
  factory CartModel({
    required final String restaurantId,
    required final List<CartItemModel> items,
  }) = _CartModel;
}
