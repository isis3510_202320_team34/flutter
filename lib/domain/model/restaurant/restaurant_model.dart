import 'package:freezed_annotation/freezed_annotation.dart';

part 'restaurant_model.freezed.dart';

@freezed
class RestaurantModel with _$RestaurantModel {
  static const String storagePath = "restaurants";

  factory RestaurantModel({
    required String id,
    required String name,
    required String address,
    required String category,
    required String description,
    required String imageName,
    required bool isOpen,
    required double latitude,
    required double longitude,
    required String phone,
    required double rate,
    required String schedule,
    @Default(-1) double distance,
  }) = _RestaurantModel;
}
