import 'package:freezed_annotation/freezed_annotation.dart';
part 'user_model.freezed.dart';

@freezed
class UserModel with _$UserModel {
  static const String storagePath = "users";
  const factory UserModel({
    required String id,
    required String name,
    required String email,
    required String imageUrl,
    required DateTime createdAt,
    required DateTime updatedAt,
  }) = _UserModel;
}
