import 'package:freezed_annotation/freezed_annotation.dart';
part 'review_model.freezed.dart';

@freezed
class ReviewModel with _$ReviewModel {
  const factory ReviewModel({
    required String id,
    required String content,
    required String userId,
    required String restaurantId,
    required double rate,
    required DateTime createdAt,
    required DateTime updatedAt,
    @Default([]) List<String> imagesUrl,
  }) = _ReviewModel;
}
