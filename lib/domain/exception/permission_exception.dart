class PermissionException implements Exception {
  String cause;
  PermissionException(this.cause);

  @override
  String toString() {
    return "No Tenemos los permisos requeridos por parte de tu dispositivo";
  }
}
